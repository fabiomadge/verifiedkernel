
theory Sign
  imports Context
begin


(* I expanded the function composition, as I had some experience where proving with it became ugly *)
definition "tsig_of theory \<equiv> tsig (signature theory)"

definition "certify_typ theory T \<equiv> Type.cert_typ (tsig_of theory) T"
definition "certify_term theory T \<equiv> undefined" (* A whole other rabbit hole *)
                                         
abbreviation "certify_class thy c == Type.cert_class' (tsig_of thy) c"

hide_const (open) Type.of_sort Sorts.of_sort
definition "of_sort thy ty S \<equiv> Type.of_sort (tsig_of thy) ty S"



end