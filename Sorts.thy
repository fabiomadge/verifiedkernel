

(* Some stuff on sorts. Mostly from Sort.ML I think. *)

theory Sorts
  imports Term
begin

(* 
  Is class \<Rightarrow> class \<Rightarrow> bool better model for classes?
  Maybe use a map for arities?
*)
(* arities should maybe return a set? *)
datatype "algebra" = Algebra 
  (classes: "class rel")
  (arities: "name \<rightharpoonup> (class \<rightharpoonup> sort list)")

definition "empty_algebra = Algebra {} Map.empty"

definition [simp]: "wf_classes rel = (trans rel \<and> acyclic rel)"

locale wf_classes_loc =
  fixes cs :: "class rel"
  assumes wf[simp]: "wf_classes cs"
begin 

definition [simp]: "class_les c1 c2 = ((c1,c2) \<in> cs)"

(* If I go for function representation here, shouldn't I do the same in the underlying *)
lemma class_les_irrefl: "\<not> class_les c c" using wf by (auto simp add: acyclic_def)
lemma class_les_trans: "class_les x y \<Longrightarrow> class_les y z \<Longrightarrow> class_les x z"
  using wf by (simp add: trans_def) (* Do this properly *)

definition [simp]: "class_leq c1 c2 = (c1=c2 \<or> class_les c1 c2)"

lemma class_leq_refl: "class_leq c c" by simp 
lemma class_leq_trans: "class_leq x y \<Longrightarrow> class_leq y z \<Longrightarrow> class_leq x z"
  using class_les_trans by auto
lemma class_leq_antisym: "class_leq c1 c2 \<Longrightarrow> class_leq c2 c1 \<Longrightarrow> c1=c2" 
  using class_leq_refl wf apply (auto simp add: trans_def)
  by (meson acyclic_def trancl.r_into_trancl)

interpretation class_order: order class_leq class_les
  apply standard 
  using class_leq_antisym class_les_irrefl apply auto[1]
  using class_leq_refl apply blast 
  using class_leq_trans apply blast
  using class_leq_antisym apply blast
  done

(* Noticing more and more disconnect between relation and functional representation, probably unify *)

(* classes form a partial order with class_les/class_leq a for a well-formed a*)

(* This should define a quasiorder on sorts, *)
definition [simp]: "sort_leq s1 s2 = (\<forall>c2\<in>s2 . \<exists>c1\<in>s1. class_leq c1 c2)"
lemma sort_leq_refl: "sort_leq s s" by auto
lemma sort_leq_trans: "sort_leq x y \<Longrightarrow> sort_leq y z \<Longrightarrow> sort_leq x z"
  using class_leq_trans wf by (meson sort_leq_def)


(* ... *)
definition [simp]: "sort_les s1 s2 = (sort_leq s1 s2 \<and> \<not> sort_leq s2 s1)"

(* Cannot instantiate as I am in a locale, however this might give sledgehammer some more lemmas to 
  work with while in here *)
interpretation sort_preorder: preorder sort_leq sort_les
  apply standard
  using sort_les_def apply blast
  using sort_leq_refl apply blast
  using sort_leq_trans by blast

lemma superset_imp_sort_leq: "s1 \<supseteq> s2 \<Longrightarrow> sort_leq s1 s2" 
  using class_leq_refl by auto
lemma full_sort_top: "sort_leq s {}" by simp

(* Is this even useful? *)
lemma sort_les_trans: "sort_les x y \<Longrightarrow> sort_les y z \<Longrightarrow> sort_les x z"
  using sort_preorder.less_trans by blast

definition [simp]: "sort_eqv s1 s2 = (sort_leq s1 s2 \<and> sort_leq s2 s1)"

lemma sort_eqvI: "sort_leq s1 s2 \<Longrightarrow> sort_leq s2 s1 \<Longrightarrow> sort_eqv s1 s2" by simp
lemma sort_eqv_refl: "sort_eqv s s" using sort_leq_refl by auto
lemma sort_eqv_trans: "sort_eqv x y \<Longrightarrow> sort_eqv y z \<Longrightarrow> sort_eqv x z"
  using sort_leq_trans wf apply simp by (metis (full_types) transD)
lemma sort_eqv_sym: "sort_eqv x y \<Longrightarrow> sort_eqv y x"
  by auto
(* sort_eqv a is equivalence relation.. *)

definition [simp]: "normalize_sort s = {c | c . c\<in>s \<and> \<not> (\<exists>c'\<in>s . class_les c' c)}"

lemma "normalize_sort {} = {}" by simp
lemma "normalize_sort (normalize_sort s) = normalize_sort s" by auto

lemma 
  assumes nempty: "s\<noteq>{}" and finite: "finite s"
  shows "normalize_sort s \<noteq> {}"
  using finite nempty apply (induction s)
  using nempty apply simp
  using wf apply simp
    apply (clarsimp simp add: acyclic_def)
  by (metis empty_iff transD)

lemma assumes elem: "c\<in>s" and nelem: "c\<notin>normalize_sort s"
  obtains c' where "c'\<in>s" and "class_les c' c"
  using elem nelem by auto

(* clean up proof *)
lemma normalize_ex_bound: 
  assumes "finite s" and elem: "c\<in>s" and nelem: "c\<notin>normalize_sort s"
  obtains c' where "c'\<in>normalize_sort s" and "class_les c' c"
  using assms 
  apply (induction s arbitrary: c)
  apply auto  
     apply (meson acyclic_def local.wf trancl.r_into_trancl wf_classes_def)
  apply (metis (full_types) local.wf transD wf_classes_def)
   apply (metis (no_types, hide_lams) class_les_def class_order.dual_order.strict_trans class_order.less_irrefl)
  by (metis (no_types, hide_lams) class_les_def class_order.order.irrefl class_order.order.strict_trans)

lemma "sort_leq s (normalize_sort s)" by auto

lemma "normalize_sort s \<subseteq> s" by simp

lemma assumes "finite s1" shows "sort_eqv s1 (normalize_sort s1)" 
proof (intro sort_eqvI[])
  show "sort_leq s1 (normalize_sort s1)" by auto
next
  show "sort_leq (normalize_sort s1) s1"
  proof (unfold sort_leq_def; intro ballI)
    fix c2 assume "c2\<in>s1"
    show "\<exists>c1\<in>normalize_sort s1. class_leq c1 c2"
    proof (cases "c2\<in>normalize_sort s1")
      case True
      then show ?thesis by simp
    next
      case False
      from this obtain c' where "c'\<in>normalize_sort s1" and "class_les c' c2" 
        using \<open>c2 \<in> s1\<close> assms normalize_ex_bound by blast
      then show ?thesis by auto 
    qed 
  qed
qed

definition [simp]: "wf_arities arss = (\<forall>ars \<in> (ran arss) .
  \<forall>c\<^sub>1 \<in> (dom ars). \<forall>c\<^sub>2 \<in> (dom ars).
  (class_leq c\<^sub>1 c\<^sub>2 \<longrightarrow> list_all2 sort_leq (the (ars c\<^sub>1)) (the (ars c\<^sub>2))))" 
  (* Encode coregularity here *)


end

(* Do I need to make sure that arities and classes talk about the same things? *)
definition [simp]: "wf_algebra a = (wf_classes (classes a) 
  \<and> wf_classes_loc.wf_arities (classes a) (arities a))"


(* I should probably just use a fold instead of this mutual recursion *)
fun insert_typ :: "typ \<Rightarrow> sort set \<Rightarrow> sort set" 
  and insert_typs :: "typ list \<Rightarrow> sort set \<Rightarrow> sort set" where
  "insert_typ (TFree _ S) Ss = insert S Ss"
| "insert_typ (TVar _ S) Ss = insert S Ss"
| "insert_typ (Type _ Ts) Ss = insert_typs Ts Ss"

| "insert_typs [] Ss = Ss"
| "insert_typs (T # Ts) Ss = insert_typs Ts (insert_typ T Ss)"

definition "sorts_of T \<equiv> insert_typ T {}"

fun insert_term :: "term \<Rightarrow> sort set \<Rightarrow> sort set" where
  "insert_term (Const _ T) Ss = insert_typ T Ss"
| "insert_term (Free _ T) Ss = insert_typ T Ss"
| "insert_term (Var _ T) Ss = insert_typ T Ss"
| "insert_term (Bound _) Ss = Ss"
| "insert_term (Abs _ T t) Ss = insert_term t (insert_typ T Ss)"
| "insert_term (t $ u) Ss = insert_term t (insert_term u Ss)"

(* try fold *)
definition "insert_terms ts Ss = fold insert_term ts Ss"
fun insert_terms' where 
  "insert_terms' [] Ss = Ss"
| "insert_terms' (t # ts) Ss = insert_terms' ts (insert_term t Ss)"
lemma "insert_terms ts Ss = insert_terms' ts Ss"
  by (induction ts arbitrary: Ss) (auto simp add: insert_terms_def)

(* This should probably go into the locale *)

(* 
  Maybe sets weren't a good idea... 
  Now I need to use a finite fold instead of pattern matching 
  Should I go back to ordered lists of classes?
  
  fun inter_class :: "algebra \<Rightarrow> class \<Rightarrow> sort \<Rightarrow> sort" where

  Dumb solution: The "intersection" of two sorts is just taking the union of the representing sets
  This should give the same sort as the ML version (modulo normaliizing of course)
*)


definition [simp]: "inter_sort (A :: algebra) (S1 :: sort) S2 = (S1 \<union> S2)"

(* Sanity lemmas... *)

(* I definitely need mre setup to make the locale usable *)
lemma 
  assumes "wf_algebra A"
  shows "wf_classes_loc.sort_leq (classes A) (inter_sort A S1 S2) S1"
  using assms
  by (simp add: wf_classes_loc.intro wf_classes_loc.superset_imp_sort_leq)

lemma 
  assumes "wf_algebra A"
  shows "wf_classes_loc.sort_leq (classes A) (inter_sort A S1 S2) S2"
  using assms
  by (simp add: wf_classes_loc.intro wf_classes_loc.superset_imp_sort_leq)

abbreviation "lookup_class A a c \<equiv> Option.bind (arities A a) (Let c)"
abbreviation "intersect_sort_vector A \<equiv> lift2_option (map2 (inter_sort A))"

(* 
  For fucks sake... Built on F from Lattices_Big
  
  I do not want to think anymore....
*)           
lemma inter_sort_comp_fun_commute: "comp_fun_commute (inter_sort A)"
  unfolding comp_fun_commute_def by auto

lemma map2_inter_sort_comp_fun_commute: "comp_fun_commute (map2 (inter_sort A))"
  unfolding comp_fun_commute_def apply (rule allI)+ subgoal for x y apply (rule ext) apply simp
    subgoal for z
      
      apply (induction x arbitrary: y z) apply auto
      subgoal for a x y z
      apply (induction y arbitrary: a x z) apply auto subgoal for b y a x z
          apply (induction z arbitrary: ) by auto 
        done
      done
    done
  done

lemma dom_inter_internal_comp_fun_commute: "comp_fun_commute (intersect_sort_vector A)"
  unfolding comp_fun_commute_def 
  apply (rule allI)+ subgoal for x y apply (rule ext) apply simp
    subgoal for z
      
      apply (induction x arbitrary: y z) apply (auto)
      apply (metis lift2_option_None)
      subgoal for a y z
      apply (induction y arbitrary: a x z) apply auto subgoal for b a z
          apply (induction z arbitrary: ) apply auto 
          subgoal for c
            apply (induction a arbitrary: b c) apply auto subgoal for x y b c
              apply (induction b arbitrary: y z) apply auto subgoal for a b y
                  apply (induction c) apply auto done
        done
      done
    done
  done
  done
  done
  done

lemma dom_inter_internal_comp_fun_idem:
  "comp_fun_idem (intersect_sort_vector A)"
  unfolding comp_fun_idem_def apply auto 
  using dom_inter_internal_comp_fun_commute apply blast
  unfolding comp_fun_idem_axioms_def apply auto subgoal for x apply (rule ext) apply simp
    subgoal for y
       apply (induction x arbitrary: y) apply auto
      subgoal for a y
        apply (induction y) apply auto subgoal for b 
          apply (induction a arbitrary: b) apply auto
      subgoal for x y b
      apply (induction b) by auto
        done
      done
    done
  done
  done

lemma map2_sym: "(\<And>x y . f x y = f y x) \<Longrightarrow> map2 f xs ys = map2 f ys xs"
  apply (induction xs arbitrary: ys) apply auto subgoal for a xs ys
  apply (induction ys arbitrary:) by auto 
  done
lemma dom_inter_internal_symmetric: "intersect_sort_vector A x y = intersect_sort_vector A y x"
  apply (cases x; cases y) apply auto 
  subgoal for as bs using map2_sym 
    using sup_commute by blast
  done

(* 
  Using the thing in the distribution for Max Min
  Maybe follow this instead: http://isabelle.informatik.tu-muenchen.de/~nipkow/pubs/tphols05.pdf

  God is this ugly
*)
interpretation dom_inter_internal_l: semilattice "intersect_sort_vector A" 
proof(standard)
  fix a b c
  show "intersect_sort_vector A (intersect_sort_vector A a b) c =
       intersect_sort_vector A a (intersect_sort_vector A b c)"
    using dom_inter_internal_symmetric dom_inter_internal_comp_fun_commute
    by (simp add: comp_fun_commute.fun_left_comm) 
next
  fix a b
  show "intersect_sort_vector A a b =
           intersect_sort_vector A b a"
    using dom_inter_internal_symmetric by blast
next
  fix a
  show "intersect_sort_vector A a a = a" apply (cases a) apply auto 
      subgoal for x apply (induction x) apply auto
        by (smt case_prod_beta map_eq_conv map_fst_zip map_snd_zip sup.idem)
      done
qed

(*<<<<<<< HEAD
(*should remove redundant classes*)
definition "inter_sort algebra S\<^sub>1 S\<^sub>2 = normalize_sort (classes algebra) (S\<^sub>1 \<inter> S\<^sub>2)"

fun mg_domain :: "algebra \<Rightarrow> name \<Rightarrow> sort \<Rightarrow> sort list" where
  "mg_domain algebra a S = (
    let interV = map2 (inter_sort algebra) in
    let ars = (the \<circ> (the ((arities algebra) a))) ` S in
    let fst = SOME x . x \<in> ars in
      Finite_Set.fold interV fst (ars - {fst}))"

fun of_sort' :: "algebra \<Rightarrow> typ \<Rightarrow> sort \<Rightarrow> bool" and of_sort :: "algebra \<Rightarrow> typ \<Rightarrow> sort \<Rightarrow> bool" where
  "of_sort' algebra (TFree _ S) S' = sort_leq (classes algebra) S S'"
| "of_sort' algebra (TVar _ S) S' = sort_leq (classes algebra) S S'"
| "of_sort' algebra (Type a Ts) S = list_all2 (of_sort algebra) Ts (mg_domain algebra a S)"

| "of_sort algebra T S = (S = {} \<or> of_sort' algebra T S)"
=======*)
interpretation sort_vector_l: semilattice_set "intersect_sort_vector A"
  by standard

lemma "finite X \<Longrightarrow> x \<notin> X 
  \<Longrightarrow> sort_vector_l.F A ({x}\<union>X) = Finite_Set.fold (intersect_sort_vector A) x X"
proof (induction X arbitrary: x rule: finite_induct)
  case empty
  then show ?case using sort_vector_l.singleton by simp
next
  case (insert f F)
  then show ?case
  proof -
    have "\<forall>Z Za z za. insert (za::char list set list option) (insert z (Za \<union> Z)) = insert z (insert za (Z \<union> Za))"
      by fastforce
    then have "\<forall>f z Z za Za. (semilattice_set.F f (insert (z::char list set list option) (insert za (Z \<union> Za))) = Finite_Set.fold f za (insert z (Za \<union> Z)) \<or> \<not> semilattice_set f) \<or> infinite (Za \<union> Z)"
      by (metis (no_types) finite_insert semilattice_set.eq_fold)
    then have "\<forall>z f za Z. (semilattice_set.F f (insert (z::char list set list option) (insert za Z)) = Finite_Set.fold f za (insert z Z) \<or> \<not> semilattice_set f) \<or> infinite Z"
      by (metis (no_types) boolean_algebra_cancel.sup0 sup_commute)
    then show ?thesis
      using sort_vector_l.semilattice_set_axioms local.insert(1) by auto
  qed
qed

fun mg_domain :: "algebra \<Rightarrow> string \<Rightarrow> sort \<Rightarrow> sort list option" where
  "mg_domain A a S = (if S={} then None else 
    (sort_vector_l.F A (lookup_class A a ` S)))"

(* I am somewhat differing from the distribution here, sort_le should take whole algebra *)
(* Cannot pattern-match on sets, so do it in a wrapper *)
fun of_sort' :: "algebra \<Rightarrow> typ \<Rightarrow> sort \<Rightarrow> bool" and of_sort :: "algebra \<Rightarrow> typ \<Rightarrow> sort \<Rightarrow> bool" where
  "of_sort' A (TFree _ S) S' = wf_classes_loc.sort_leq (classes A) S S'"
| "of_sort' A (TVar _ S) S' = wf_classes_loc.sort_leq (classes A) S S'"
| "of_sort' A (Type a Ts) S = (case mg_domain A a S of 
    None \<Rightarrow> False 
  | Some Ss \<Rightarrow> list_all2 (of_sort A) Ts Ss)"

| "of_sort A T S = (S = {} \<or> of_sort' A T S)"

end