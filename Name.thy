
(* 
  I do not really want to formalize this entire module...
  But here we go
*)

theory Name
  imports Preliminaries "HOL-Library.Sublist"
begin


(** common defaults **)

abbreviation "uu \<equiv> ''uu''"
abbreviation "uui \<equiv> ''uu_''"
abbreviation "aT \<equiv> '''a''"


(** special variable names **)

(* encoded bounds *)

(*names for numbered variables --
  preserves order wrt. int_ord vs. string_ord, avoids allocating new strings*)

(* I just realized I probably won't need this.. *)
(*
(* Define this... *)
consts string_of_int :: "int \<Rightarrow> string"
abbreviation "small_int \<equiv> map 
  (\<lambda>i. CHR '':''#(if i < 10 then ''00'' else if i < 100 then ''0'' else '''' )@string_of_int i) 
  (upto 0 1000)"

fun bound n =
  if n < 1000 then Vector.sub (small_int, n)
  else ":" ^ bound (n div 1000) ^ Vector.sub (small_int, n mod 1000);

val is_bound = String.isPrefix ":";
*)

(* internal names -- NB: internal subsumes skolem *)

find_consts "'a list \<Rightarrow> 'a list"

abbreviation "internal \<equiv> (\<lambda>s. s @ ''_'')"

abbreviation "dest_internal s \<equiv> if suffix ''_'' s then Some (butlast s) else None"

lemma dest_internal_shortens: "dest_internal s = Some s' \<Longrightarrow> length s' < length s"
  apply (induction s arbitrary: s')
  apply auto
  by (metis add_diff_cancel_left' length_Cons length_butlast less_Suc_eq option.discI 
      option.inject plus_1_eq_Suc)
  
(* Don't need them now
val is_internal = String.isSuffix "_";
fun reject_internal (x, ps) =
  if is_internal x then error ("Bad name: " ^ quote x ^ Position.here_list ps) else ();

val skolem = suffix "__";
val dest_skolem = unsuffix "__";
val is_skolem = String.isSuffix "__";
fun reject_skolem (x, ps) =
  if is_skolem x then error ("Bad name: " ^ quote x ^ Position.here_list ps) else ();
*)

function clean_index :: "string \<times> int \<Rightarrow> string \<times> int" where
  "clean_index (x, i) = (case dest_internal x of 
    None \<Rightarrow> (x, i) 
  | Some x' \<Rightarrow> clean_index (x', i +1))"
  by auto
termination by (relation "measure (\<lambda>(x,i). length x)") (auto simp add: dest_internal_shortens)

abbreviation "clean x \<equiv> fst (clean_index (x, 0))"

(** generating fresh names **)

(* context *)

datatype "context" =
  Context "string \<rightharpoonup> string option"    (*declared names with latest renaming*)

(* fun does not terminate so primrec for the pattern matching,
  probably lots of cases generated for string argument somehow? *)
primrec "declare" :: "string \<Rightarrow> Name.context \<Rightarrow> Name.context" where
  "declare x (Context tab) = 
    Context ((case tab (clean x) of Some _ \<Rightarrow> tab | None \<Rightarrow> tab(clean x \<mapsto> None)))"

primrec declare_renaming :: "string \<Rightarrow> string \<Rightarrow> Name.context \<Rightarrow> Name.context" where
  "declare_renaming x x' (Context tab) = Context (tab(clean x \<mapsto> Some (clean x')))"

primrec is_declared where "is_declared (Context tab) x = (\<not> Option.is_none (tab x))"
primrec declared where "declared (Context tab) x = tab x"

(* How do I write "'" nicer in inner syntax? *)
abbreviation "context \<equiv> fold declare ['''', [CHR 0x27]] (Context Map.empty)"
abbreviation "make_context used \<equiv> fold declare used context"

(* invent names *)

(* Do I need this for now? 
ML_val\<open>Symbol.bump_string\<close>
fun invent ctxt =
  let
    fun invs _ 0 = []
      | invs x n =
          let val x' = Symbol.bump_string x
          in if is_declared ctxt x then invs x' n else x :: invs x' (n - 1) end;
  in invs o clean end;

fun invent_names ctxt x xs = invent ctxt x (length xs) ~~ xs;

val invent_list = invent o make_context;
*)

(* variants *)

(*makes a variant of a name distinct from already used names in a
  context; preserves a suffix of underscores "_"*)

(* Define *)
consts bump_string :: "string \<Rightarrow> string"

(* Problem: terminates only for "finite" contexts, there at some point all collisions are avoided
  For general contexts, not so much 
*) 
function (domintros) vary :: "Name.context \<Rightarrow> string \<Rightarrow> string" where
  "vary ctxt x = (case declared ctxt x of 
    None \<Rightarrow> x 
  | Some x' \<Rightarrow> vary ctxt (bump_string (the_default x x')))" 
  by auto
thm vary.domintros
(* TODO: a termination proof here, maybe only partial *)
(* TODO:

fun variant name ctxt =
  let
    fun vary x =
      (case declared ctxt x of
        NONE => x
      | SOME x' => vary (Symbol.bump_string (the_default x x')));

    val (x, n) = clean_index (name, 0);
    val (x', ctxt') =
      if not (is_declared ctxt x) then (x, declare x ctxt)
      else
        let
          val x0 = Symbol.bump_init x;
          val x' = vary x0;
          val ctxt' = ctxt
            |> x0 <> x' ? declare_renaming (x0, x')
            |> declare x';
        in (x', ctxt') end;
  in (x' ^ replicate_string n "_", ctxt') end;

fun variant_list used names = #1 (make_context used |> fold_map variant names);
*)

(* names conforming to typical requirements of identifiers in the world outside *)

(* Probably won't be needing this
fun enforce_case' false cs =
      (if forall Symbol.is_ascii_upper cs then map else nth_map 0) Symbol.to_ascii_lower cs
  | enforce_case' true cs =
      nth_map 0 Symbol.to_ascii_upper cs;

fun enforce_case upper = implode o enforce_case' upper o raw_explode;

fun desymbolize perhaps_upper "" =
      if the_default false perhaps_upper then "X" else "x"
  | desymbolize perhaps_upper s =
      let
        val xs as (x :: _) = Symbol.explode s;
        val ys =
          if Symbol.is_ascii_letter x orelse Symbol.is_symbolic x then xs
          else "x" :: xs;
        fun is_valid x =
          Symbol.is_ascii_letter x orelse Symbol.is_ascii_digit x;
        fun sep [] = []
          | sep (xs as "_" :: _) = xs
          | sep xs = "_" :: xs;
        fun desep ("_" :: xs) = xs
          | desep xs = xs;
        fun desymb x xs =
          if is_valid x then x :: xs
          else
            (case Symbol.decode x of
              Symbol.Sym name => "_" :: raw_explode name @ sep xs
            | _ => sep xs);
        val upper_lower = Option.map enforce_case' perhaps_upper |> the_default I;
      in fold_rev desymb ys [] |> desep |> upper_lower |> implode end;
*)

end