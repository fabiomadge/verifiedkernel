
theory Context
  imports Term Type Sorts
begin

(* 
  Definitions here instead of their own theories because of dependency relations
  I do not allow adding arbitrary data to a theory, so everything must be known before
*)

(* Abstracted signature, for proofs I feel like we are getting far to many layers *)
datatype sign = Sign (tsig: tsig) ("consts": "string \<Rightarrow> typ option")


(* 
  I propose removing the "generic data storage" from the theory specification and just include the
  needed data directly.
*)
datatype "theory" = Theory (signature: "sign") (axioms: "term set")

end