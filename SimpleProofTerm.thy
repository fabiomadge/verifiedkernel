
(*
  Fully "constructed" simple proof terms for Isabelle/Pure.
  Currently no support for previously proofed theorems and oracles, 
  no omitted parts and no compression
*)

theory SimpleProofTerm
  imports Term AbstractPure
begin

datatype proofterm = PAxm "term" "typ list" | PBound nat | Hyp "term"
  | Abst string "typ" proofterm | AbsP string "term" proofterm
  | Appt proofterm "term" | AppP proofterm proofterm
  | OfClass "typ" "class"
  (* Oracle, PThm *)

(* Move *)
type_synonym tyinst = "(indexname \<times> sort) \<times> typ" 
type_synonym tinst = "(indexname \<times> typ) \<times> term" 

ML_val\<open>Sign.of_sort\<close>

fun handle_atom :: "theory \<Rightarrow> term \<Rightarrow> typ list \<Rightarrow> term option" where
  "handle_atom


fun replay :: "theory \<Rightarrow> tyinst list \<times> tinst list \<Rightarrow> term set \<Rightarrow> proofterm \<Rightarrow> (term set \<times> term) option" where
  "replay thy (tyinst, tinst) Hs t = undefined"
end