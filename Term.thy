
theory Term
  imports Main Preliminaries
begin

(* Types directly from the ML code, added some curry. *)
type_synonym name = string
type_synonym indexname = "name \<times> int"

type_synonym "class" = "string"
(* Does this abstraction make sense? "Normal form" of sorts is duplicate free, ordered list in dist*)
type_synonym "sort" = "class set"

datatype "typ" =
  is_Type: Type name "typ list" |
  is_TVar: TVar indexname sort |
  is_TFree: TFree string sort

value[nbe] "un_Type1 (Type ''a'' [])"

datatype "term" =
  Const string "typ" |
  Free name "typ" |
  Var indexname "typ" |
  Bound nat |
  Abs string "typ" "term" | (* The string parameter should only be there for readability, it should not have any function in the logic, right? If so, remove it ? *)
  App "term" "term" (infixl "$" 100)


(* Not really using this currently, was there for experiments of reducing all to Vars *)
fun embed_name_indexname :: "string \<Rightarrow> indexname" where
  "embed_name_indexname s = (s,-1)"
fun is_embeded_name :: "indexname \<Rightarrow> bool" where
  "is_embeded_name (_,i) = (i = -1)"

abbreviation "mk_fun_typ S T \<equiv> Type ''fun'' [S,T]" 
notation mk_fun_typ (infixr "\<rightarrow>" 100)

(* Maybe overload those definitions for code closer to the papers? *)

(* names and code currently based on kuncar and popescu paper, 
  probably should move a bit closer to isabelle/ML here
  Those would be the "add_X" from term.ML
  Some definitions for frees are missing, but they are analogous to the ones for vars
 *)
fun TVarsT :: "typ \<Rightarrow> (indexname * sort) set" where 
  "TVarsT (TVar a s) = {(a,s)}"
| "TVarsT (TFree _ _) = {}"
| "TVarsT (Type _ tys) = \<Union>(set (map TVarsT tys))"

fun TVars :: "term \<Rightarrow> (indexname * sort) set" where
  "TVars (Const _ T) = TVarsT T"
| "TVars (Var _ T) = TVarsT T"
| "TVars (Free _ T) = TVarsT T"
| "TVars (Bound _) = {}"
| "TVars (Abs _ T body) = TVarsT T \<union> TVars body"
| "TVars (f $ u) = TVars f \<union> TVars u"

fun TFreesT :: "typ \<Rightarrow> (name * sort) set" where
  "TFreesT (TVar _ _) = {}"
| "TFreesT (TFree a s) = {(a,s)}"
| "TFreesT (Type _ tys) = \<Union>(set (map TFreesT tys))"

fun TFrees :: "term \<Rightarrow> (name * sort) set" where
  "TFrees (Const _ T) = TFreesT T"
| "TFrees (Var _ T) = TFreesT T"
| "TFrees (Free _ T) = TFreesT T"
| "TFrees (Bound _) = {}"
| "TFrees (Abs _ T body) = TFreesT T \<union> TFrees body"
| "TFrees (f $ u) = TFrees f \<union> TFrees u"

abbreviation "TVars_set S \<equiv> \<Union>t\<in>S . TVars t"
abbreviation "TFrees_set S \<equiv> \<Union>t\<in>S . TFrees t"

fun TVar_namesT :: "typ \<Rightarrow> indexname set" where 
  "TVar_namesT (TVar a _) = {a}"
| "TVar_namesT (TFree _ _) = {}"
| "TVar_namesT (Type _ tys) = \<Union>(set (map TVar_namesT tys))"

lemma TVar_namesT_alternate_def: "TVar_namesT \<tau> = fst ` (TVarsT \<tau>)"
  by (induction \<tau>) auto 

fun TVar_names :: "term \<Rightarrow> indexname set" where
  "TVar_names (Const _ T) = TVar_namesT T"
| "TVar_names (Var _ T) = TVar_namesT T"
| "TVar_names (Free _ T) = TVar_namesT T"
| "TVar_names (Bound _) = {}"
| "TVar_names (Abs _ T body) = TVar_namesT T \<union> TVar_names body"
| "TVar_names (f $ u) = TVar_names f \<union> TVar_names u"

lemma TVar_names_alternate_def: "TVar_names t = fst ` (TVars t)"
  by (induction t) (auto simp add: TVar_namesT_alternate_def)

abbreviation "TVar_names_set S \<equiv> \<Union>t\<in>S . TVar_names t"

lemma "TVar_names_set ts = fst ` (TVars_set ts)"
  using TVar_names_alternate_def by auto

fun Vars :: "term \<Rightarrow> (indexname * typ) set" where
  "Vars (Const _ _) = {}"
| "Vars (Var idx T) = {(idx, T)}"
| "Vars (Free _ _) = {}"
| "Vars (Bound _) = {}"
| "Vars (Abs _ _ body) = Vars body"
| "Vars (f $ u) = Vars f \<union> Vars u"

fun Frees :: "term \<Rightarrow> (name * typ) set" where
  "Frees (Const _ _) = {}"
| "Frees (Var _ _) = {}"
| "Frees (Free n T) = {(n, T)}"
| "Frees (Bound _) = {}"
| "Frees (Abs _ _ body) = Frees body"
| "Frees (f $ u) = Frees f \<union> Frees u"

fun Variables :: "term \<Rightarrow> term set" where
  "Variables (Const _ _) = {}"
| "Variables (Bound _) = {}"
| "Variables (Abs _ _ body) = Variables body"
| "Variables (f $ u) = Variables f \<union> Variables u"
| "Variables t = {t}"

fun is_variable  :: "term \<Rightarrow> bool" where
  "is_variable (Free _ _) = True"
| "is_variable (Var _ _) = True"
| "is_variable _ = False"

lemma "(\<lambda>(x,s) . Var x s) ` (Vars t) \<subseteq> Variables t"
  by (induction t) auto

lemma Variables_is_variables: "\<forall>x\<in>Variables t. is_variable x"
  by (induction t) auto

(*
  For now assume we must just instantiate Vars. That's what they are there for, right 
  Can of course all be transfered to frees as well
*)
fun tsubstT :: "typ \<Rightarrow> (indexname \<Rightarrow> sort \<Rightarrow> typ) \<Rightarrow> typ" where 
  "tsubstT (TVar a s) \<rho> = \<rho> a s"
| "tsubstT (TFree a s) \<rho> = TFree a s"
| "tsubstT (Type k \<sigma>s) \<rho> = Type k (map (\<lambda>\<sigma>. tsubstT \<sigma> \<rho>) \<sigma>s)"


(* Somewhat misusing the types, should probably make an Either input type *)
fun tsubstT' :: "typ \<Rightarrow> (typ \<Rightarrow> typ) \<Rightarrow> typ" where 
  "tsubstT' (TVar a s) \<rho> = \<rho> (TVar a s)"
| "tsubstT' (TFree a s) \<rho> = \<rho> (TFree a s)"
| "tsubstT' (Type k \<sigma>s) \<rho> = Type k (map (\<lambda>\<sigma>. tsubstT' \<sigma> \<rho>) \<sigma>s)"

lemma TVarsT_tsubstT: "TVarsT (tsubstT \<sigma> \<rho>) = \<Union> {TVarsT (\<rho> a s) | a s. (a, s) \<in> TVarsT \<sigma>}"
  by (induction \<sigma>) fastforce+ 

lemma tsubstT_cong: 
  "(\<forall>(a,s) \<in> TVarsT \<sigma>. \<rho>1 a = \<rho>2 a) \<Longrightarrow> tsubstT \<sigma> \<rho>1 = tsubstT \<sigma> \<rho>2"
  by (induction \<sigma>) fastforce+ 

lemma tsubstT_ith: "i < length Ts \<Longrightarrow>  map (\<lambda>T . tsubstT T \<rho>) Ts ! i = tsubstT (Ts ! i) \<rho>" by simp

lemma tsubstT_fun_typ_dist: "tsubstT (T\<rightarrow>T1) \<rho> = tsubstT T \<rho> \<rightarrow> tsubstT T1 \<rho>" 
    by (simp add: )

fun tsubst :: "term \<Rightarrow> (indexname \<Rightarrow> sort \<Rightarrow> typ) \<Rightarrow> term" where
  "tsubst (Const s T) \<rho> = Const s (tsubstT T \<rho>)" (* I should be able to substitute here, right? *)
| "tsubst (Var s T) \<rho> = Var s (tsubstT T \<rho>)"
| "tsubst (Free s T) \<rho> = Free s (tsubstT T \<rho>)"
| "tsubst (Bound i) _ = Bound i"
| "tsubst (Abs s T body) \<rho> = Abs s (tsubstT T \<rho>) (tsubst body \<rho>)"
| "tsubst (f $ u) \<rho> = tsubst f \<rho> $ tsubst u \<rho>"

fun tsubst' :: "term \<Rightarrow> (typ \<Rightarrow> typ) \<Rightarrow> term" where
  "tsubst' (Const s T) \<rho> = Const s (tsubstT' T \<rho>)" (* I should be able to substitute here, right? *)
| "tsubst' (Var s T) \<rho> = Var s (tsubstT' T \<rho>)"
| "tsubst' (Free s T) \<rho> = Free s (tsubstT' T \<rho>)"
| "tsubst' (Bound i) _ = Bound i"
| "tsubst' (Abs s T body) \<rho> = Abs s (tsubstT' T \<rho>) (tsubst' body \<rho>)"
| "tsubst' (f $ u) \<rho> = tsubst' f \<rho> $ tsubst' u \<rho>"

fun subst :: "term \<Rightarrow> (indexname \<Rightarrow> typ \<Rightarrow> term) \<Rightarrow> term" where
  "subst (Const s T) \<rho> = Const s T"
| "subst (Var s T) \<rho> = \<rho> s T"
| "subst (Free s T) \<rho> = Free s T"
| "subst (Bound i) _ = Bound i"
| "subst (Abs s T body) \<rho> = Abs s T (subst body \<rho>)"
| "subst (f $ u) \<rho> = subst f \<rho> $ subst u \<rho>"


fun subst' :: "term \<Rightarrow> (term \<Rightarrow> term) \<Rightarrow> term" where
  "subst' (Const s T) \<rho> = Const s T"
| "subst' (Var s T) \<rho> = \<rho> (Var s T)"
| "subst' (Free s T) \<rho> = \<rho> (Free s T)"
| "subst' (Bound i) _ = Bound i"
| "subst' (Abs s T body) \<rho> = Abs s T (subst' body \<rho>)"
| "subst' (f $ u) \<rho> = subst' f \<rho> $ subst' u \<rho>"

(*
  inductive and functional versions of typ_of. Probably should use only the functional versions,
  but inductive was closer to paper
*)

inductive has_typ1 :: "typ list \<Rightarrow> term \<Rightarrow> typ \<Rightarrow> bool" where
  "has_typ1 _ (Const _ T) T"
| "i < length Ts \<Longrightarrow> has_typ1 Ts (Bound i) (nth Ts i)"
| "has_typ1 _ (Var _ T) T"
| "has_typ1 _ (Free _ T) T"
| "has_typ1 (T#Ts) body T1 \<Longrightarrow> has_typ1 Ts (Abs _ T body) (T \<rightarrow> T1)"
| "has_typ1 Ts u U \<Longrightarrow> has_typ1 Ts f (U \<rightarrow> R) \<Longrightarrow>
      has_typ1 Ts (f $ u) R"

definition "has_typ t ty = has_typ1 [] t ty"

lemma has_typ1_weaken_Ts: "has_typ1 Ts t ty \<Longrightarrow> has_typ1 (Ts@[T]) t ty"
  apply (induction arbitrary: rule: has_typ1.induct) apply (auto intro: has_typ1.intros)
  by (metis butlast_snoc has_typ1.intros(2) length_append_singleton less_Suc_eq nth_butlast)

fun typ_of1 :: "typ list \<Rightarrow> term \<Rightarrow> typ option" where
  "typ_of1 _ ( Const _ T) = Some T"
| "typ_of1 Ts (Bound i) = (if i < length Ts then Some (nth Ts i) else None)" (*handling here*)
| "typ_of1 _ (Var _ T) = Some T"
| "typ_of1 _ (Free _ T) = Some T"
| "typ_of1 Ts (Abs _ T body) = Option.bind (typ_of1 (T#Ts) body) (\<lambda>x. Some (T \<rightarrow> x))"
| "typ_of1 Ts (f $ u) = Option.bind (typ_of1 Ts u) (\<lambda>U. Option.bind (typ_of1 Ts f) (\<lambda>T. 
    case T of
          Type fun [T1,T2] \<Rightarrow> if fun = ''fun'' then
            if T1=U then Some T2 else None
            else None
        | _ \<Rightarrow> None
    ))"

definition "typ_of t \<equiv> typ_of1 [] t"

lemma typ_of_split_app: "typ_of f = Some (\<alpha> \<rightarrow> \<beta>) \<Longrightarrow> typ_of x = Some \<alpha> \<Longrightarrow> typ_of (f $ x) = Some \<beta>"
  by (simp add: typ_of_def)

lemma typ_of_split_abs: "typ_of1 (\<alpha>#Ts) t = Some \<beta> \<Longrightarrow> v = \<alpha> \<Longrightarrow> typ_of1 Ts (Abs s v t) = Some (\<alpha> \<rightarrow> \<beta>)"
  by (simp add: typ_of_def)

lemma has_typ1_imp_typ_of1:  "has_typ1 Ts t ty \<Longrightarrow> typ_of1 Ts t = Some ty"
  by (induction rule: has_typ1.induct) auto

lemma typ_of1_imp_has_typ1: "typ_of1 Ts t = Some ty \<Longrightarrow> has_typ1 Ts t ty"
proof (induction t arbitrary: Ts ty)
  case (App t u)  
  from this obtain U where U: "typ_of1 Ts u = Some U" by fastforce
  from this App obtain T where T: "typ_of1 Ts t = Some T" by fastforce
  from U T App obtain T2 where "T = Type ''fun'' [U, T2]"
    by (auto simp add: bind_eq_Some_conv intro!: has_typ1.intros split: if_splits typ.splits list.splits)
  from this U T show ?case using App by (auto intro!: has_typ1.intros(6))
qed (auto simp add: bind_eq_Some_conv intro!: has_typ1.intros split: if_splits)

corollary has_typ1_iff_typ_of1: "has_typ1 Ts t ty \<longleftrightarrow> typ_of1 Ts t = Some ty" 
  using has_typ1_imp_typ_of1 typ_of1_imp_has_typ1 by blast

corollary has_typ_iff_typ_of: "has_typ t ty \<longleftrightarrow> typ_of t = Some ty"
  by (simp add: has_typ1_iff_typ_of1 has_typ_def typ_of_def)

lemma typ_of1_weaken_Ts: "typ_of1 Ts t = Some ty \<Longrightarrow> typ_of1 (Ts@[T]) t = Some ty"
  using has_typ1_weaken_Ts by (simp add: has_typ1_iff_typ_of1)

(* Instantiation of type variables produces instantiated types *)
lemma has_typ1_tsubst: 
  "has_typ1 Ts t T \<Longrightarrow> has_typ1 (map (\<lambda>T. tsubstT T \<rho>) Ts) (tsubst t \<rho>) (tsubstT T \<rho>)"
proof (induction rule: has_typ1.induct)
  case (2 i Ts)
  (* tsubst_ith must be applied reversed, in this direction it can cause simplifier to loop *)
  then show ?case using tsubstT_ith by (metis has_typ1.intros(2) length_map tsubst.simps(4))
qed (auto simp add: tsubstT_fun_typ_dist intro: has_typ1.intros)

corollary has_typ1_unique: 
  assumes "has_typ1 \<tau>s t \<tau>1" and "has_typ1 \<tau>s t \<tau>2" shows "\<tau>1 = \<tau>2"
  using assms
  by (metis has_typ1_imp_typ_of1 option.inject)

fun aconv :: "term \<Rightarrow> term \<Rightarrow> bool" where
  "aconv (t1 $ u1) (t2 $ u2) \<longleftrightarrow> aconv t1 t2 \<and> aconv u1 u2"
| "aconv (Abs _ T1 t1) (Abs _ T2 t2) \<longleftrightarrow> aconv t1 t2 \<and> T1 = T2"
| "aconv a1 a2 \<longleftrightarrow> a1 = a2"

(* aconv gives a equivalence relation on terms *)
lemma aconv_sym: "aconv t1 t2 \<Longrightarrow> aconv t2 t1"
  by (induction rule: aconv.induct) auto
lemma aconv_refl: "aconv t t"
  by (induction t) auto
lemma aconv_trans: "aconv t1 t2 \<Longrightarrow> aconv t2 t3 \<Longrightarrow> aconv t1 t3"
proof (induction arbitrary: t2 rule: aconv.induct)
  case (1 f1 u1 f2 u2)
  then show ?case by (cases t2) auto 
next
  case (2 uu T1 t1 uv T2 t2)
  then show ?case by (cases t2) auto
next
  case ("3_9" v va vb vc vd)
  then show ?case by (cases t2) auto
next
  case ("3_18" vc vd v va vb)
  then show ?case by (cases t2) auto
qed simp_all

lemma aconv_has_typ1': "has_typ1 Ts t1 T \<Longrightarrow> aconv t1 t2 \<Longrightarrow> has_typ1 Ts t2 T"
proof (induction arbitrary: t2 rule: has_typ1.induct)
next
  case (5 T Ts body T1 s)
  then show ?case by (cases t2) (auto intro: has_typ1.intros)
next
  case (6 Ts u U f R)
  then show ?case by (cases t2) (auto intro: has_typ1.intros)
qed (cases t2, auto simp add: tsubstT_fun_typ_dist intro: has_typ1.intros)

lemma aconv_has_typ1: "aconv t1 t2 \<Longrightarrow> has_typ1 Ts t1 T \<longleftrightarrow> has_typ1 Ts t2 T"
  using aconv_has_typ1' aconv_sym by auto

lemma "aconv t1 t2 \<Longrightarrow> typ_of1 Ts t1 = typ_of1 Ts t2"
  using aconv_has_typ1 has_typ1_iff_typ_of1 by (metis not_Some_eq)

fun loose_bvar :: "term \<Rightarrow> nat \<Rightarrow> bool" where
  "loose_bvar (Bound i) k \<longleftrightarrow> i \<ge> k"
| "loose_bvar (f $ t) k \<longleftrightarrow> loose_bvar f k \<or> loose_bvar t k"
| "loose_bvar (Abs _ _ t) k = loose_bvar t (k+1)"
| "loose_bvar _ _ = False"

definition "is_open t \<equiv> loose_bvar t 0"

abbreviation "is_closed t \<equiv> \<not> is_open t"

lemma loose_bvar_Suc: "loose_bvar t (Suc k) \<Longrightarrow> loose_bvar t k"
  by (induction t arbitrary: k) auto
lemma "k\<ge>p \<Longrightarrow> loose_bvar t k \<Longrightarrow> loose_bvar t p"
  by (induction rule: inc_induct) (use loose_bvar_Suc in auto)

lemma has_typ1_imp_no_loose_bvar: "has_typ1 Ts t ty \<Longrightarrow> \<not> loose_bvar t (length Ts)"
  by (induction rule: has_typ1.induct) auto

corollary has_typ_imp_closed: "has_typ t ty \<Longrightarrow> \<not> is_open t"
  unfolding is_open_def has_typ_def using has_typ1_imp_no_loose_bvar by fastforce

corollary typ_of_imp_closed: "typ_of t = Some ty \<Longrightarrow> \<not> is_open t"
  by (simp add: has_typ_iff_typ_of has_typ_imp_closed)

(* Implemented exception handling with options... Some helper functions to have less case madness *)
fun abstract_over1 :: "term \<Rightarrow> nat \<Rightarrow> term \<Rightarrow> term option" where
  "abstract_over1 v lev tm = (if aconv v tm then Some (Bound lev) else 
    (case tm of
      Abs a T t \<Rightarrow> map_option (Abs a T) (abstract_over1 v (lev + 1) t)
    | (t $ u) \<Rightarrow> 
        (lift2_option App (abstract_over1 v lev t) (abstract_over1 v lev u OR Some u) 
          OR map_option (App t) (abstract_over1 v lev u)
        )
    | _ \<Rightarrow> None))"
(* This produces dangling Bound, only use when enclosing term in Abs *)
definition "abstract_over v body = (case abstract_over1 v 0 body of Some res \<Rightarrow> res | None \<Rightarrow> body)"

(* 
  Proof is mostly hammered without thinking. I could probably reduce the whole App case with some
  more clever automation, but no more thinking
  It is marvelous that Isabelle even manages to get a somewhat sensible induction rule 
  for my definition. 
  I should define one that includes the whole case splitting directly
  
*)
lemma abstract_over1_change: 
  "\<not> loose_bvar tm lev \<Longrightarrow> abstract_over1 v lev tm = Some tm' \<Longrightarrow> tm \<noteq> tm'"
proof(induction arbitrary: tm' rule: abstract_over1.induct)
  case (1 v lev tm)
  then show ?case
  proof (cases "aconv v tm")
    case True
    then show ?thesis using 1
      by (metis (no_types, lifting) Term.term.simps(20) Term.term.simps(26) 
          abstract_over1.elims eq_iff loose_bvar.elims(3) option.sel term.distinct(25) 
          term.distinct(27) term.distinct(5) term.inject(4))
  next
    case False
    then show ?thesis
    proof (cases tm)
      case (Abs x51 x52 x53)
      then show ?thesis using False 1(1) "1.prems"(1) "1.prems"(2) by fastforce
    next
      case (App t u)
      then show ?thesis
      proof (cases "lift2_option App (abstract_over1 v lev t) (abstract_over1 v lev u OR Some u)")
        case None
        then show ?thesis using 1 False App
          by (smt Term.term.simps(42) abstract_over1.simps loose_bvar.simps(2) 
              map_option_case option.expand option.map_sel option.sel option.simps(4) 
              option.simps(9) term.inject(6))
      next
        case (Some comb')
        note outer = this
        then show ?thesis
        proof (cases "abstract_over1 v lev u")
          case None
          note outer' = this
          show ?thesis using 1 False App Some None 
          proof (cases "abstract_over1 v lev t")
            case None
            then show ?thesis using 1 False App Some outer'
              by (metis lift2_option.simps(1) option.discI)
          next
            case (Some t')
            hence "lift2_option App (abstract_over1 v lev t) (abstract_over1 v lev u OR Some u)
              = Some (App t' u)" using outer' by simp
            then show ?thesis using 1 False App outer outer' 
              by (metis (no_types, lifting) Some Term.term.simps(42) abstract_over1.elims 
                  loose_bvar.simps(2) option.sel option.simps(5) term.inject(6))
          qed
        next
          case (Some u')
          note outer' = this
          then show ?thesis
          proof (cases "abstract_over1 v lev t")
            case None
            then show ?thesis using 1 False App Some outer'
              by (metis lift2_option.simps(1) option.discI outer)
          next
            case (Some t')
            hence "lift2_option App (abstract_over1 v lev t) (abstract_over1 v lev u OR Some u)
              = Some (App t' u')" using outer' by simp
            then show ?thesis using 1 False App outer outer' 
              by (metis (no_types, lifting) Some Term.term.simps(42) abstract_over1.elims 
                  loose_bvar.simps(2) option.sel option.simps(5) term.inject(6))
          qed
        qed
      qed
    qed (metis (no_types, lifting) False 1 Term.term.simps abstract_over1.elims option.discI)+
  qed
qed


(* Again, probably ugly for proofs... *)
fun exists_subterm :: "(term \<Rightarrow> bool) \<Rightarrow> term \<Rightarrow> bool" where
  "exists_subterm P t \<longleftrightarrow> P t \<or> (case t of
      (t $ u) \<Rightarrow> exists_subterm P t \<or> exists_subterm P u
    | Abs s ty body \<Rightarrow> exists_subterm P body
    | _ \<Rightarrow> False)"
(* Is this better? *)
fun exists_subterm' :: "(term \<Rightarrow> bool) \<Rightarrow> term \<Rightarrow> bool" where
  "exists_subterm' P (t $ u) \<longleftrightarrow> P (t $ u) \<or> exists_subterm' P t \<or> exists_subterm' P u"
| "exists_subterm' P (Abs s ty body) \<longleftrightarrow> P (Abs s ty body) \<or> exists_subterm' P body"
| "exists_subterm' P t \<longleftrightarrow>  P t"
thm exists_subterm.induct exists_subterm'.induct
lemma exists_subterm_iff_exists_subterm': "exists_subterm P t \<longleftrightarrow> exists_subterm' P t"
  by (induction t) auto

lemma "exists_subterm (\<lambda>t. t=Var idx T) t \<longleftrightarrow> (idx, T) \<in> Vars t"
  by (induction t) auto

(* Must have no loose bounds in t, from Logic.ML*)
abbreviation "occs t u \<equiv> exists_subterm (\<lambda>s. aconv t s) u"

(* 
  Again, might need to adjust induction rule
*)
lemma abstract_over1_unchanged: 
  "abstract_over1 v lev tm = None \<Longrightarrow> \<not> occs v tm"
proof (induction rule: abstract_over1.induct)
  case (1 v lev tm)
  then show ?case
  proof (cases "aconv v tm")
    case True
    then show ?thesis using 1 by auto
  next
    case False
    then show ?thesis
    proof (cases tm)
      case (App t u)
      then show ?thesis using False 1 by (fastforce simp add: Or_None)
    qed (use False 1 in auto)
  qed
qed

corollary abstract_over_unchanged: "\<not> is_open tm \<Longrightarrow> abstract_over v tm = tm \<Longrightarrow> \<not> occs v tm"
  unfolding is_open_def abstract_over_def using abstract_over1_unchanged 
  by (metis abstract_over1_change option.case_eq_if option.collapse)

(* Cleanup needed again*)
lemma abstract_over1_changed: 
  "abstract_over1 v lev tm = Some tm' \<Longrightarrow> occs v tm"
proof (induction arbitrary: tm' rule: abstract_over1.induct)
  case (1 v lev tm)
  then show ?case
  proof (cases "aconv v tm")
    case True
    then show ?thesis using 1 by auto
  next
    case False
    then show ?thesis
    proof (cases tm)
      case (Abs s x body)
      then show ?thesis using False "1.prems" "1.IH"(1) by auto
    next
      case (App t u)
      then show ?thesis using False 1 by fastforce
    qed (use False 1 in auto)
  qed 
qed

corollary abstract_over_changed: "\<not> is_open tm \<Longrightarrow> abstract_over v tm ~= tm \<Longrightarrow> occs v tm"
  unfolding is_open_def abstract_over_def using abstract_over1_changed
  by (metis option.case_eq_if option.collapse)

(* Convenience *)

abbreviation "aT S == TFree ''a'' S"

abbreviation "itselfT ty == Type ''itself'' [ty]"
abbreviation "a_itselfT == itselfT (TFree ''a'' empty)"

abbreviation "constT name \<equiv> Type name []"
abbreviation "propT \<equiv> constT ''prop''"

abbreviation "mk_eq t1 t2 \<equiv> Const ''Pure.eq'' (the (typ_of t1) \<rightarrow> the (typ_of t2) \<rightarrow> propT) $ t1 $ t2"

abbreviation mk_imp :: "term \<Rightarrow> term \<Rightarrow> term"  (infixl "\<longmapsto>" 51) where "A \<longmapsto> B \<equiv> Const ''Pure.imp'' (propT \<rightarrow> (propT \<rightarrow> propT)) $ A $ B"

lemma 
  assumes "has_typ A propT" and "has_typ B propT" 
  shows "has_typ (mk_imp A B) propT"
  using assms by (auto simp add: has_typ_def intro: has_typ1.intros)


(* 
  Another one, by now that induction rule really would have helped  
  Could do the same with Var. Maybe just abstract over arbitrary term. But then I need to make 
  sure that I handle contained bounds correctly

  Try more functional version, as a lot of hammer proofs seem to take the detour
  Seems to have been a success

  Other wise those elimination rules seem to be easier to show with has_typ
  But maybe I am just tired

  Currently getting the feeling that I should just induct on the term <- this is the solution
  The structure of abstract_over1 is just the term structure
*)
inductive_cases has_typ1_app_elim: "has_typ1 Ts (f $ u) R" thm has_typ1_app_elim
lemma has_typ1_arg_typ: "has_typ1 Ts (f $ u) R \<Longrightarrow> has_typ1 Ts u U \<Longrightarrow> has_typ1 Ts f (U \<rightarrow> R)"
  using has_typ1_app_elim by (metis has_typ1_unique)
 
lemma has_typ1_fun_typ: "has_typ1 Ts (f $ u) R \<Longrightarrow> has_typ1 Ts f (U \<rightarrow> R) \<Longrightarrow> has_typ1 Ts u U"
  apply (cases rule: has_typ1_app_elim[of Ts f u R "has_typ1 Ts u U"]) apply simp
  using has_typ1_unique by blast

lemma typ_of1_arg_typ: 
  "typ_of1 Ts (f $ u) = Some R \<Longrightarrow> typ_of1 Ts u = Some U \<Longrightarrow> typ_of1 Ts f = Some (U \<rightarrow> R)"
  using has_typ1_iff_typ_of1 has_typ1_arg_typ by simp
lemma typ_of1_fun_typ: 
  "typ_of1 Ts (f $ u) = Some R \<Longrightarrow> typ_of1 Ts f = Some (U \<rightarrow> R) \<Longrightarrow> typ_of1 Ts u = Some U "
  using has_typ1_iff_typ_of1 has_typ1_fun_typ by blast

(* 
  Hack'd together version that works for both Vars and Frees, 
  proof is just a bunch of case distinctions more
  Need some better automation setup and that should disapear
*)
lemma abstract_over1_preserves_type_gen:
  assumes "typ_of1 Ts t = Some ty" 
    and "abstract_over1 v (length Ts) t = Some t'"
    and "typ_of1 Ts v = Some T" and "is_variable v"
  shows "typ_of1 (Ts@[T]) t' = Some ty"
  using assms
proof (induction t arbitrary: v T t' Ts ty)
  case (Abs s bT body)
    from this  obtain iT where iT: "typ_of1 (bT#Ts) body = Some iT" 
      by (auto simp add: bind_eq_Some_conv)
    from this Abs have ty: "ty = bT \<rightarrow> iT" 
      by (auto simp add: bind_eq_Some_conv)
    from Abs obtain body' where 
      body': "abstract_over1 v (length Ts + 1) body = Some body'" using Abs.prems by (cases v) auto
      
    from this Abs have t': "t' = Abs s bT body'" apply simp by (cases v) auto
  
    have "typ_of1 ((bT # Ts) @ [T]) body' = Some iT" 
      using Abs.IH typ_of1_weaken_Ts body' iT apply simp apply (cases v) using Abs apply simp_all
      apply (smt Abs.IH Abs.prems(3) Abs.prems(4) add.commute append_Cons body' length_Cons plus_1_eq_Suc typ_of1.simps(4))
      apply (smt Abs.IH Abs.prems(3) Abs.prems(4) add.commute append_Cons body' length_Cons plus_1_eq_Suc typ_of1.simps(3))
      done

    then show ?case using Abs ty t' by simp
next
  case (App f u)

  have weakened: "typ_of1 (Ts @ [T]) (App f u) = Some ty" using App typ_of1_weaken_Ts by blast

  obtain argT where u_ty: "typ_of1 Ts u = Some argT" 
    using App by (auto simp add: bind_eq_Some_conv) 
  hence f_ty: "typ_of1 Ts f = Some (argT \<rightarrow> ty)" 
    using App typ_of1_arg_typ by blast

  have no_aconv: "\<not>aconv v (App f u)" using App apply (cases v) by simp_all

  then show ?case 
  proof (cases "abstract_over1 (v) (length Ts) f")
    case None
    note f_None = None
    then show ?thesis 
    proof (cases "abstract_over1 (v) (length Ts) u")
      case None
      hence "abstract_over1 (v) (length Ts) (App f u) = None" using App f_None apply (cases v) by auto
      then show ?thesis using App.prems(2) by auto
    next
      case (Some u')
      hence "abstract_over1 (v) (length Ts) (App f u) = Some (App f u')" using App f_None apply (cases v) by auto
      then show ?thesis using App Some typ_of1_weaken_Ts u_ty 
        by (smt option.inject typ_of1.simps(6))(*That should be just splitting and explicit combining*)
    qed
  next
    case (Some f')
    note f_Some = Some
    then show ?thesis
    proof (cases "abstract_over1 (v) (length Ts) u")
      case None
      hence "abstract_over1 (v) (length Ts) (App f u) = Some (App f' u)" 
        using App f_Some None apply (cases v) by auto
      then show ?thesis using App Some typ_of1_weaken_Ts u_ty f_ty by auto
    next
      case (Some u')
      hence "abstract_over1 (v) (length Ts) (App f u) = Some (App f' u')" 
        using App f_Some apply (cases v) by auto
      then show ?thesis using App Some typ_of1_weaken_Ts u_ty f_ty 
        using bind_eq_Some_conv f_Some option.sel by auto
    qed
  qed
qed (auto split: if_splits)

corollary abstract_over1_preserves_type:
  assumes "typ_of1 Ts t = Some ty" 
    and "abstract_over1 (Free n T) (length Ts) t = Some t'"
  shows "typ_of1 (Ts@[T]) t' = Some ty"
  using abstract_over1_preserves_type_gen assms
  using is_variable.simps(1) typ_of1.simps(4) by blast

corollary abstract_over1_preserves_type':
  assumes "typ_of1 Ts t = Some ty" 
    and "abstract_over1 (Var n T) (length Ts) t = Some t'"
  shows "typ_of1 (Ts@[T]) t' = Some ty"
  using abstract_over1_preserves_type_gen assms
  using is_variable.simps(2) typ_of1.simps(3) by blast

(* 
  Problem here: Must indicate that no change means that no capturing can take place
  Need a exist_subterm predicate, something similar is in ML aswell I think
  should probably do pure Abs abstract_over first
*)

abbreviation "mk_Abstraction s v t \<equiv> Abs s (the (typ_of v)) (abstract_over v t)"

lemma typ_of_Abs_abstract_over_gen:
  assumes "typ_of A = Some ty"
    and "typ_of1 Ts v = Some bT" and "is_variable v"
  shows "typ_of (Abs s bT (abstract_over v A)) = Some (bT \<rightarrow> ty)"
proof(cases "abstract_over v A = A")
  case True
  (* 
    Cleanup
  *)
  have step: "[bT] = []@[bT]" by simp
  from True show ?thesis using assms apply (auto simp add: typ_of_def)
    apply (subst step) using typ_of1_weaken_Ts by fastforce
next
  case False
  from this obtain A' where A': "abstract_over1 v 0 A = Some A'"
    using abstract_over_def by fastforce
  hence "typ_of1 [bT] A' = Some ty" using abstract_over1_preserves_type_gen assms typ_of_def
    by (metis is_variable.elims(2) list.size(3) self_append_conv2 typ_of1.simps(3) typ_of1.simps(4))
  then show ?thesis unfolding typ_of_def abstract_over_def 
    apply (subst A') by auto 
qed

corollary typ_of_Abs_abstract_over:
  assumes "typ_of A = Some ty"
  shows "typ_of (Abs s bT (abstract_over (Free s bT) A)) = Some (bT \<rightarrow> ty)"
  using typ_of_Abs_abstract_over_gen assms by simp

corollary typ_of_Abs_abstract_over':
  assumes "typ_of A = Some ty"
  shows "typ_of (Abs s bT (abstract_over (Var (s,i) bT) A)) = Some (bT \<rightarrow> ty)"
  using typ_of_Abs_abstract_over_gen assms by simp


corollary typ_of_mk_Abstraction:
  assumes "typ_of A = Some ty" and "typ_of v = Some bT" and "is_variable v"
  shows "typ_of (mk_Abstraction s v A) = Some (bT \<rightarrow> ty)"
  using assms typ_of_Abs_abstract_over_gen typ_of_def
  by (metis option.sel)


(* I could maybe just allow abstracting over arbitrary terms to make this uniform *)
abbreviation "mk_all x ty t \<equiv> 
  Const ''Pure.all'' ((ty \<rightarrow> propT) \<rightarrow> propT) $ mk_Abstraction x (Free x ty) t"
abbreviation "mk_all' x ty t \<equiv> 
  Const ''Pure.all'' ((ty \<rightarrow> propT) \<rightarrow> propT) $ mk_Abstraction (fst x) (Var x ty) t"


lemma
  assumes "typ_of A = Some propT" 
  shows "typ_of (mk_all x ty A) = Some propT"
  using typ_of_Abs_abstract_over[OF assms, of x ty] by (auto simp add: typ_of_def)

(* For beta application, needs some lemmas *)

fun incr_bv :: "nat \<Rightarrow> nat \<Rightarrow> term \<Rightarrow> term" where
  "incr_bv inc lev (Bound i) = (if i >= lev then Bound (i+inc) else Bound i)"
| "incr_bv inc lev (Abs a T body) = Abs a T (incr_bv inc (lev+1) body)"
| "incr_bv inc lev (App f t) = App (incr_bv inc lev f) (incr_bv inc lev t)"
| "incr_bv _ _ u = u"

lemma "incr_bv 0 lev t = t"
  by (induction t arbitrary: lev) auto

lemma "loose_bvar t lev \<Longrightarrow> loose_bvar (incr_bv inc lev t) (lev+inc)"
  apply (induction t arbitrary: inc lev)
  apply auto
  by fastforce

fun incr_boundvars :: "nat \<Rightarrow> term \<Rightarrow> term" where
  "incr_boundvars 0 t = t"
| "incr_boundvars inc t = incr_bv inc 0 t"

fun subst_bound1 :: "term \<Rightarrow> nat \<Rightarrow> term \<Rightarrow> term option" where
  "subst_bound1 (Bound i) lev arg = (if i < lev then None
    else if i = lev then Some (incr_boundvars lev arg)
    else Some (Bound (i - 1)))"
| "subst_bound1 (Abs a T body) lev arg = map_option (Abs a T) (subst_bound1 body (lev + 1) arg)"
| "subst_bound1 (f $ t) lev arg = lift2_option App
    (subst_bound1 f lev arg) 
    (subst_bound1 t lev arg OR Some t)
    OR map_option (App f) (subst_bound1 t lev arg)" (* Same pattern as in abstract_over1, extract*)
| "subst_bound1 _ _ _ = None"

definition "subst_bound arg t \<equiv> case subst_bound1 t 0 arg of None \<Rightarrow> t | Some res \<Rightarrow> res"

lemma no_loose_bvar_imp_no_subst_bound1: "\<not>loose_bvar t lev \<Longrightarrow> subst_bound1 t lev arg = None"
  by (induction t arbitrary: lev) auto

corollary closed_subst_bound_no_change: "is_closed t \<Longrightarrow> subst_bound arg t = t"
  unfolding is_open_def subst_bound_def no_loose_bvar_imp_no_subst_bound1 by simp

fun betapply :: "term \<Rightarrow> term \<Rightarrow> term" (infixl "\<bullet>" 52) where
  "betapply (Abs _ _ t) u = subst_bound u t"
| "betapply f u = f $ u"

(* Refactor this proof, much duplication and hammering *)
lemma subst_bound1_abstract_over1: 
  assumes "abstract_over1 v lev t = Some t'" and "\<not> loose_bvar t lev" and "is_variable v" 
  shows "subst_bound1 t' lev v = Some t"
  using assms
  proof (induction t arbitrary: v lev t')
    case (Const x1 x2)
    then show ?case by (cases v) (auto simp add: abstract_over_def subst_bound_def)
  next
    case (Free x1 x2)
    then show ?case apply (cases v) 
           apply (auto simp add: abstract_over_def subst_bound_def split: if_splits)
      apply (cases lev) by simp_all 
  next
    case (Var x1 x2)
    then show ?case apply (cases v) 
           apply (auto simp add: abstract_over_def subst_bound_def split: if_splits)
      apply (cases lev) by simp_all 
  next
    case (Bound x)
    then show ?case apply (cases v) by (auto simp add: abstract_over_def subst_bound_def split: if_splits)
  next
    case (Abs x1 x2 t)
    then show ?case apply (cases v) by (auto simp add: abstract_over_def subst_bound_def split: if_splits)
  next
    case (App t1 t2)
    from this(3) this(5) obtain t1' t2' where t': "t' = App t1' t2'"
      apply (cases v) 
           apply (auto simp add: Or_Some lift2_option_not_None split: )
      using lift2_option.elims apply blast+ done
    show ?case
    proof (cases "abstract_over1 v lev t1")
      case None note None1 = this
      then show ?thesis
      proof (cases "abstract_over1 v lev t2")
        case None 
        then show ?thesis using None1 App by (auto simp add: t' Or_Some lift2_option_not_None split: if_splits)
      next
        case (Some t2'')
        hence "t1' = t1" using None1 App 
          by (auto simp add: t' Or_Some lift2_option_not_None split: if_splits)
        hence 1: "subst_bound1 t1' lev v = None" using no_loose_bvar_imp_no_subst_bound1
          using App.prems(2) by auto
          
        from Some have "t2''  = t2'" using None1 App 
          by (auto simp add:t' Or_Some lift2_option_not_None split: if_splits)
        hence 2: "subst_bound1 t2' lev v = Some t2" using App.IH(2) App.prems
          using Some loose_bvar.simps(2) by blast
        show ?thesis
          using 1 2  \<open>t1' = t1\<close> by (auto simp add: t' Or_Some lift2_option_not_None)
      qed
    next
      case (Some t1'') note Some1 = this
      then show ?thesis
      proof (cases "abstract_over1 v lev t2")
        case None
        from Some1 have "t1'' = t1'" using None App 
          by (auto simp add: t' Or_Some lift2_option_not_None split: if_splits)
        hence 1: "subst_bound1 t1' lev v = Some t1" using App.IH(1) App.prems
          using Some loose_bvar.simps(2) by blast

        have "t2' = t2" using App.prems t' None Some 
          by (auto simp add: Or_Some lift2_option_not_None split: if_splits)
        hence 2: "subst_bound1 t2' lev v = None" using no_loose_bvar_imp_no_subst_bound1
          using App.prems(2) by auto

        then show ?thesis using 1 2 \<open>t2' = t2\<close> by (auto simp add: t' Or_Some lift2_option_not_None)
      next
        case (Some t2'')
        have "abstract_over1 v lev (t1 $ t2) = Some (t1'' $ t2'')"
          using App.prems t' Some1 Some by (auto simp add: Or_Some lift2_option_not_None split: if_splits)
        hence "t1'' = t1'" "t2'' = t2'" using App.prems t' by simp_all
        moreover hence "subst_bound1 t1' lev v = Some t1" "subst_bound1 t2' lev v = Some t2" 
          using App.IH App.prems Some1 Some by simp_all
        ultimately show ?thesis by (auto simp add: t' Or_Some lift2_option_not_None)
      qed
    qed
  qed

corollary subst_bound_abstract_over: 
  assumes "is_variable v" and "is_closed t"
  shows "subst_bound v (abstract_over v t) = t"
  unfolding abstract_over_def subst_bound_def using assms subst_bound1_abstract_over1 is_open_def
  by (metis no_loose_bvar_imp_no_subst_bound1 not_Some_eq option.simps(4) option.simps(5))

lemma 
  assumes "is_closed t" and "typ_of v = Some T" and "is_variable v" 
  shows "betapply (mk_Abstraction s v t) v = t"
using assms subst_bound_abstract_over by simp

(*Substitute new for free occurrences of old in a term*)
(* Show that I can simulate subst' and this function with each other, at least for Variables *)
fun subst_free :: "(term \<times> term) list \<Rightarrow> term \<Rightarrow> term" where
  "subst_free [] t = t"
| "subst_free pairs t = (case List.find (\<lambda>(x,_) . aconv t x) pairs of
    Some (_, t') \<Rightarrow> t'
  | None \<Rightarrow> (case t of 
      Abs s T t \<Rightarrow> Abs s T (subst_free pairs t)
    | f $ u \<Rightarrow>  subst_free pairs f $ subst_free pairs u
    | _ \<Rightarrow> t))"
(* Ugly, but things should be doable by induction over the term parameter *)
thm subst_free.induct

(* 
  Represent partial functions from variables (classes) to terms 
  I need association lists for this, probably build or import a theory
  term * term elements
  First component needs to be a variable
  List must be distinct up to aconv in first argument <- together with previous this means 
  distinct by equality, See next lemma
*)
lemma aconv_variables_equal:
  "is_variable a \<Longrightarrow> is_variable b \<Longrightarrow> aconv a b \<Longrightarrow> a=b" by (cases a) simp_all 


lemma length_subst_free_pairs:
  assumes "distinct (map fst pairs)" and "distinct (map fst pairs')" and "set pairs = set pairs'"
  shows "length pairs = length pairs'"
  using assms        
  by (metis distinct_card length_map set_map)


lemma subst_free_Const_unchanged:
  assumes "distinct (map fst pairs)" and "list_all is_variable (map fst pairs)"
  shows "subst_free pairs (Const s T) = (Const s T)" 
  using assms apply (cases pairs)
   apply (auto split: prod.splits term.splits)
  by (smt find_None_iff image_eqI is_variable.simps(3) list.set_map list_all_iff option.case_eq_if prod.case_eq_if)
  

lemma subst_free_Boud_unchanged:
  assumes "distinct (map fst pairs)" and "list_all is_variable (map fst pairs)"
  shows "subst_free pairs (Bound x) = (Bound x)" 
  using assms apply (cases pairs)
   apply (auto split: prod.splits term.splits)
  by (smt find_None_iff image_eqI is_variable.simps(4) list.set_map list_all_iff option.case_eq_if prod.case_eq_if)

lemma subst_free_Const_changed1:
  assumes "distinct (map fst pairs)" and "list_all is_variable (map fst pairs)"
    and "(Free s T, t) \<in> set pairs"
  shows "subst_free pairs (Free s T) = t" 
  using assms apply (cases pairs) apply simp_all
   apply (auto split: prod.splits term.splits simp add: rev_image_eqI)
  using find_the_pair option.simps(5) by fastforce

lemma subst_free_Const_changed2:
  assumes "distinct (map fst pairs)" and "list_all is_variable (map fst pairs)"
    and "(Var s T, t) \<in> set pairs"
  shows "subst_free pairs (Var s T) = t" 
  using assms apply (cases pairs) apply simp_all
   apply (auto split: prod.splits term.splits simp add: rev_image_eqI)
  using find_the_pair option.simps(5) by fastforce

lemma subst_free_Const_changed:
  assumes "distinct (map fst pairs)" and "list_all is_variable (map fst pairs)"
    and "(p, t) \<in> set pairs"
  shows "subst_free pairs p = t" 
  using assms apply (cases pairs) 
  apply simp using find_the_pair 
  by (smt is_variable.elims(2) list_all_iff set_zip_leftD subst_free_Const_changed1 subst_free_Const_changed2 zip_map_fst_snd)
  
lemma aconv_variables_suitable: 
  "list_all is_variable (map fst pairs) \<Longrightarrow> x\<in>set (map fst pairs) \<Longrightarrow> y\<in>set (map fst pairs)
  \<Longrightarrow> aconv t x \<Longrightarrow> aconv t y \<Longrightarrow> x = y"
  using aconv_variables_equal by (meson aconv_sym aconv_trans list_all_iff)

(* I am missing something, everything is fing ugly*)
lemma subst_free_irrelevant_order:
  assumes "distinct (map fst pairs)" and "distinct (map fst pairs')" and "set pairs = set pairs'"
  and "list_all is_variable (map fst pairs)"
shows "subst_free pairs t = subst_free pairs' t"
  using assms
proof(induction t arbitrary: )
  case (Const x1 x2)
  then show ?case using subst_free_Const_unchanged by (simp add: list_all_iff)
next
  case (Free s T)
  hence "List.find (\<lambda>(x, _). aconv (Free s T) x) pairs = List.find (\<lambda>(x, _). aconv (Free s T) x) pairs'"
    using find_pairs_order_irrel[] aconv_variables_suitable[] by blast
  then show ?case apply (cases pairs; cases pairs') apply simp_all
    by (metis Term.term.simps(38))
next
  case (Var s T)
  hence "List.find (\<lambda>(x, _). aconv (Var s T) x) pairs = List.find (\<lambda>(x, _). aconv (Var s T) x) pairs'"
    using find_pairs_order_irrel[] aconv_variables_suitable[] by blast
  then show ?case apply (cases pairs; cases pairs') apply simp_all
    by (metis Term.term.simps)
next
  case (Bound n)
  hence "List.find (\<lambda>(x, _). aconv (Bound n) x) pairs = List.find (\<lambda>(x, _). aconv (Bound n) x) pairs'"
    using find_pairs_order_irrel[] aconv_variables_suitable[] by blast
  then show ?case apply (cases pairs; cases pairs') apply simp_all
    by (metis Term.term.simps)
next
  case (Abs s T t)
  hence "List.find (\<lambda>(x, _). aconv (Abs s T t) x) pairs = List.find (\<lambda>(x, _). aconv (Abs s T t) x) pairs'"
    using find_pairs_order_irrel[] aconv_variables_suitable[] by blast
  then show ?case apply (cases pairs; cases pairs') apply simp_all
    using assms(3) apply auto
    by (metis Abs.IH Term.term.simps(41) assms(1) assms(2) assms(3) assms(4))
next
  case (App t1 t2)
  hence "List.find (\<lambda>(x, _). aconv (App t1 t2) x) pairs = List.find (\<lambda>(x, _). aconv (App t1 t2) x) pairs'"
    using find_pairs_order_irrel[] aconv_variables_suitable[] by blast
  then show ?case apply (cases pairs; cases pairs') apply simp_all
    using assms(3) apply auto
    by (metis App(1) App.IH(2) Term.term.simps(42) assms(1) assms(2) assms(3) assms(4))
qed

lemma finite_Variables: "finite (Variables t)"
  by (induction t) auto

lemma no_variables_imp_subst'_unchanged: "Variables t = {} \<Longrightarrow> subst' t \<rho> = t"
  by (induction t) auto

lemma find_Some: "List.find P l = Some p \<Longrightarrow> p\<in>set l \<and> P p"
  by (induction l) (auto split: if_splits) 

(* 
  Can simulate abstract substitutions into term variables by Pure's subst_free
  This was far more work than anticipated

  subst_free can also do more(replace arbitrary subterms not just variables)

  Like all stuff this needs cleanup... I need a long trainride soon to do this
*)
lemma subst'_subst_free: 
  "subst' t \<rho> = subst_free (map (\<lambda>x.(x, \<rho> x)) (SOME l . distinct l \<and> set l = Variables t)) t"
proof-
  obtain l where l: "set l = Variables t" "distinct l" 
    using finite_Variables finite_distinct_list by blast
  let ?l = "map (\<lambda>x.(x, \<rho> x)) l"
  let ?l' = "map (\<lambda>x.(x, \<rho> x)) (SOME l . distinct l \<and> set l = Variables t)"

  have p1: "distinct (map fst ?l)" using l by (simp add: map_idI)
  have p2: "distinct (map fst ?l')"
  proof-
    have "distinct (SOME l . distinct l \<and> set l = Variables t)"
      by (metis (mono_tags, lifting) l(1) l(2) someI)
    thus ?thesis by (simp add: map_idI)
  qed
  have p3: "set ?l = set ?l'" using l 
    by (metis (mono_tags, lifting) image_set tfl_some)
  have p4: "list_all is_variable (map fst ?l)" using l
    by (simp add: list_all_iff Variables_is_variables)
  have pexp: "Variables t \<subseteq> set l" using l by (simp add: list_all_iff Variables_is_variables)
  have pexp': "list_all is_variable l" using l
    by (simp add: list_all_iff Variables_is_variables)

  have core: "subst' t \<rho> = subst_free ?l t"
  proof(cases "Variables t = empty")
    case True
    hence "?l = []" using l by auto
    then show ?thesis using True no_variables_imp_subst'_unchanged by auto
  next
    case False
    hence l_nempty: "?l ~= []" using l by auto

    then show ?thesis using pexp l(2) p1 p4 pexp'
    proof(induction ?l t arbitrary: l rule: subst_free.induct)
      case (1 t)
      then show ?case by simp
    next
      case (2 v vs t)
      hence "list_all is_variable (map fst (v#vs))" by simp
      have "map fst (v#vs) = l" using \<open>v # vs = map (\<lambda>x. (x, \<rho> x)) l\<close>
        by (simp add: map_idI)

      then show ?case
      proof(cases t)
        case (Const x11 x12)
        then show ?thesis 
          using "2.prems"(2) 
          using "2.prems"(4) "2.prems"(5) subst_free_Const_unchanged by auto
      next
        case (Free s T)
        hence "Free s T \<in> set l" using "2.prems" by simp
        hence "Free s T \<in> set (map fst (map (\<lambda>x. (x, \<rho> x)) l))" by simp
        then show ?thesis using subst_free_Const_changed "2.prems" Free
          by (simp add: image_iff)
      next
        case (Var s T)
        hence "Var s T \<in> set l" using "2.prems" by simp
        hence "Var s T \<in> set (map fst (map (\<lambda>x. (x, \<rho> x)) l))" by simp
        then show ?thesis using subst_free_Const_changed "2.prems" Var
          by (simp add: image_iff)
      next
        case (Bound x)
        then show ?thesis using "2.prems" subst_free_Boud_unchanged by simp
      next
        case (Abs s T b)
        then show ?thesis
        proof(cases "List.find (\<lambda>(x,_) . aconv t x) (map (\<lambda>x.(x, \<rho> x)) l)")
          case None
          have "subst' b \<rho> = subst_free (map (\<lambda>x. (x, \<rho> x)) l) b"
            apply (rule 2(1)[of s T b l])
            using None "2.hyps" "2.prems" Abs by auto
          then show ?thesis
            using "2.hyps"(4) Abs None by auto
        next
          case (Some r)
          hence "r \<in> set (map (\<lambda>x.(x, \<rho> x)) l)" and "aconv t (fst r)" using find_Some by fastforce+
          hence "fst r = (Abs s T b)" 
            using "2.prems" Abs Some Variables_is_variables 
            apply auto
            by (metis (mono_tags, lifting) aconv.simps(13) aconv.simps(9) is_variable.elims(2) list_all_iff)
          then show ?thesis using subst_free_Const_changed "2.prems"
            by (metis (mono_tags, lifting) \<open>r \<in> set (map (\<lambda>x. (x, \<rho> x)) l)\<close> image_eqI image_set is_variable.simps(5) list_all_iff)       
        qed
      next
        case (App f u)
        then show ?thesis
        proof(cases "List.find (\<lambda>(x,_) . aconv t x) (map (\<lambda>x.(x, \<rho> x)) l)")
          case None
          have "subst' f \<rho> = subst_free (map (\<lambda>x. (x, \<rho> x)) l) f" thm 2(2)
            apply (rule 2(2)[of f u])
            using "2.hyps" "2.prems" App None by simp_all
          moreover have "subst' u \<rho> = subst_free (map (\<lambda>x. (x, \<rho> x)) l) u" thm 2(2)
            apply (rule 2(3)[of f u])
            using "2.hyps" "2.prems" App None by simp_all
          ultimately show ?thesis
            using "2.hyps" App None  App None \<open>v # vs = map (\<lambda>x. (x, \<rho> x)) l\<close> by auto
        next
          case (Some r)
          hence "r \<in> set (map (\<lambda>x.(x, \<rho> x)) l)" and "aconv t (fst r)" using find_Some by fastforce+
          hence "fst r = (App f u)" 
            using "2.prems"(2) App Some Variables_is_variables 
            apply auto 
            by (metis (full_types) "2.hyps"(4) "2.prems"(5) \<open>map fst (v # vs) = l\<close> aconv.simps(13) aconv.simps(5) aconv_sym is_variable.elims(2) list_all_iff)
          then show ?thesis using subst_free_Const_changed "2.prems" \<open>r \<in> set (map (\<lambda>x. (x, \<rho> x)) l)\<close>
            by (metis (mono_tags) image_iff is_variable.simps(6) list_all_iff set_map)      
        qed
      qed
    qed
  qed

  show ?thesis using p1 p2 p3 p4 subst_free_irrelevant_order core by presburger
qed

lemma "\<forall>(x,y) \<in> set pairs . is_variable x \<and> \<not> is_open y \<and> s
  \<Longrightarrow> typ_of t1 = Some ty1 \<Longrightarrow> \<exists>ty2. typ_of (subst_free pairs t1) = Some ty2"
  oops

(* 
  I should probably formalize signatures, theories and do all these proofs with certified
   terms directly, maybe I can transfer them through "certification"
*)
lemma 
  assumes "typ_of t = Some (T \<rightarrow> propT)"
    and "typ_of u = Some T"
  shows "typ_of (betapply (Const ''Pure.all'' ((T \<rightarrow> propT) \<rightarrow> propT) $ t) u) = Some ty"
  oops



(* maximum index of typs and terms *)

fun maxidx_typ_code :: "typ \<Rightarrow> int \<Rightarrow> int" and maxidx_typs_code :: "typ list \<Rightarrow> int \<Rightarrow> int" where
  "maxidx_typ_code (TVar (_, j) _) i = max i j"
| "maxidx_typ_code (Type _ Ts) i = maxidx_typs_code Ts i"
| "maxidx_typ_code (TFree _ _) i = i"
(* Maybe just fold. But would add another layer of ind rules *)
| "maxidx_typs_code [] i = i"
| "maxidx_typs_code (T # Ts) i = maxidx_typs_code Ts (maxidx_typ_code T i)"
thm maxidx_typ_code_maxidx_typs_code.induct

fun maxidx_typ_clean :: "typ \<Rightarrow> int \<Rightarrow> int" where
  "maxidx_typ_clean (TVar (_, j) _) i = max i j"
| "maxidx_typ_clean (Type _ Ts) i = fold (\<lambda>ty j . maxidx_typ_clean ty j) Ts i"
| "maxidx_typ_clean (TFree _ _) i = i"

abbreviation "maxidx_typs_clean Ts i == fold (\<lambda>ty j . maxidx_typ_clean ty j) Ts i"

(* These lemmas justify working with the simpler definitions *)
lemma "maxidx_typ_code ty i = maxidx_typ_clean ty i"
  by (induction ty i rule: maxidx_typ_code_maxidx_typs_code.induct(1)
      [where Q = "\<lambda>Ts i . maxidx_typs_code Ts i = fold maxidx_typ_clean Ts i"]) simp_all

lemma "maxidx_typs_code Ts i = maxidx_typs_clean Ts i"
  by (induction Ts i rule: maxidx_typ_code_maxidx_typs_code.induct(2)
      [where P = "\<lambda>ty i . maxidx_typ_code ty i = maxidx_typ_clean ty i"]) simp_all

abbreviation "maxidx_typ == maxidx_typ_clean"
abbreviation "maxidx_typs == maxidx_typs_clean"

value "maxidx_typ (Type ''test'' [TFree ''a'' {}, TVar (''a'', 1) {}, TVar (''a'', 3) {}]) (-1)"

fun maxidx_term :: "term \<Rightarrow> int \<Rightarrow> int" where
  "maxidx_term (Var (_, j) T) i = maxidx_typ T (max i j)"
| "maxidx_term (Const _ T) i = maxidx_typ T i"
| "maxidx_term (Free _ T) i = maxidx_typ T i"
| "maxidx_term (Bound _) i = i"
| "maxidx_term (Abs _ T t) i = maxidx_term t (maxidx_typ T i)"
| "maxidx_term (t $ u) i = maxidx_term u (maxidx_term t i)"

definition "maxidx_of_typ T \<equiv> maxidx_typ T (-1)"
definition "maxidx_of_typs Ts \<equiv> maxidx_typs Ts (-1)"
definition "maxidx_of_term t \<equiv> maxidx_term t (-1)"



(* Transfer some stuff I tried in experiments for modeling exceptions *)

(*Determines the type of a term, with minimal checking*)

fun fastype_of1 :: "typ list * term \<Rightarrow> typ option" where
  "fastype_of1 (Ts, f$u) = Option.bind (fastype_of1 (Ts,f)) (\<lambda>U. case U of
      Type fun [_,T] => (if fun = ''fun'' then Some T else None)
    | _ \<Rightarrow> None)"
| "fastype_of1 (_, Const _ T) = Some T"
| "fastype_of1 (_, Free _ T) = Some T"
| "fastype_of1 (Ts, Bound i) = (if i < length Ts then Some (nth Ts i) else None)" (*handling here*)
| "fastype_of1 (_, Var _ T) = Some T"
| "fastype_of1 (Ts, Abs _ T u) = Option.bind (fastype_of1 (T#Ts, u)) (\<lambda>x. Some (T \<rightarrow> x))"

definition "fastype_of t = fastype_of1 ([],t)"

(*Determine the argument type of a function*)

(*Using nat here as this is what is probably meant+easier pattern matching. But not exactly the same...*)
fun argT :: "nat \<Rightarrow> typ \<Rightarrow> typ option" where
  "argT i (Type fun [T, U]) = (if fun = ''fun''
    then if i = 0 then Some T else argT (i - 1) U
    else None)"
| "argT _ T = None"

fun arg :: "nat \<Rightarrow> typ list \<Rightarrow> term \<Rightarrow> typ option" where
  "arg 0 _ (Abs _ T _) = Some T"
| "arg i Ts (Abs _ T t) = arg (i - 1) (T # Ts) t"
| "arg i Ts (t $ _) = arg (i + 1) Ts t"
| "arg i Ts a = Option.bind (fastype_of1 (Ts, a)) (argT i)"

definition argument_type_of :: "term \<Rightarrow> nat \<Rightarrow> typ option" where
  "argument_type_of tm k = arg k [] tm"

fun abs where "abs (x, T) t = Abs x T t"

fun strip_abs :: "term \<Rightarrow> (string * typ) list * term" where
  "strip_abs (Abs a T t) = (let (a', t') = strip_abs t in ((a, T) # a', t'))"
| "strip_abs t = ([], t)"

(*maps  (x1,...,xn)t   to   t*)
fun strip_abs_body :: "term \<Rightarrow> term" where
  "strip_abs_body (Abs _ _ t) = strip_abs_body t"
| "strip_abs_body u = u"

(*maps  (x1,...,xn)t   to   [x1, ..., xn]*)
fun strip_abs_vars :: "term \<Rightarrow> (string*typ) list" where
  "strip_abs_vars (Abs a T t) = (a,T) # strip_abs_vars t"
| "strip_abs_vars u = []"

(*Dropped inner helper function, instead passing qnt along.  *)
fun strip_qnt_body :: "string \<Rightarrow> term \<Rightarrow> term" where
  "strip_qnt_body qnt ((Const c ty) $ (Abs _ _ t)) = 
    (if c=qnt then strip_qnt_body qnt t else (Const c ty))"
| "strip_qnt_body _ t = t"

(*Dropped inner helper function, instead passing qnt along.  *)
fun strip_qnt_vars :: "string \<Rightarrow> term \<Rightarrow> (string * typ) list" where 
  "strip_qnt_vars qnt (Const c _ $ Abs a T t)= (if c=qnt then (a,T) # strip_qnt_vars qnt t else [])"
| "strip_qnt_vars qnt t  =  []"

(*maps   (f, [t1,...,tn])  to  f(t1,...,tn)*)
definition list_comb :: "term * term list \<Rightarrow> term" where "list_comb = case_prod (foldl ($))"
(*seems more natural curried...*)
definition list_comb' :: "term \<Rightarrow> term list \<Rightarrow> term" where "list_comb' = foldl ($)"

lemma "list_comb (h,t) = list_comb' h t" by (simp add: list_comb_def list_comb'_def)

(*curry this... ?*)
fun strip_comb_imp where
  "strip_comb_imp (f$t, ts) = strip_comb_imp (f, t # ts)"
| "strip_comb_imp x = x"

(*maps   f(t1,...,tn)  to  (f, [t1,...,tn]) ; naturally tail-recursive*)
definition strip_comb :: "term \<Rightarrow> term * term list" where
  "strip_comb u = strip_comb_imp (u,[])"

(*maps   f(t1,...,tn)  to  f , which is never a combination*)
fun head_of :: "term \<Rightarrow> term" where
  "head_of (f$t) = head_of f"
| "head_of u = u"

(*some sanity check lemmas*)

lemma fst_strip_comb_imp_eq_head_of: "fst (strip_comb_imp (t,ts)) = head_of t" 
  by (induction "(t,ts)" arbitrary: t ts rule: strip_comb_imp.induct) simp_all
corollary "fst (strip_comb t) = head_of t"
  using fst_strip_comb_imp_eq_head_of by (simp add: strip_comb_def)

(*not in ML*)
fun is_app :: "term \<Rightarrow> bool" where
  "is_app (_ $ _) = True"
| "is_app _ = False"

lemma not_is_app_imp_strip_com_imp_unchanged: "\<not> is_app t \<Longrightarrow> strip_comb_imp (t,ts) = (t,ts)" 
  by (cases t) simp_all
corollary not_is_app_imp_strip_com_unchanged: "\<not> is_app t \<Longrightarrow> strip_comb t = (t,[])" 
  unfolding strip_comb_def using not_is_app_imp_strip_com_imp_unchanged .

lemma list_comb_fuse: "list_comb (list_comb (t,ts), ss) = list_comb (t,ts@ss)"
  unfolding list_comb_def by simp        
       
fun add_size_term :: "term \<Rightarrow> int \<Rightarrow> int" where
  "add_size_term (t $ u) n = add_size_term t (add_size_term u n)"
| "add_size_term (Abs _ _ t) n = add_size_term t (n + 1)"
| "add_size_term _ n = n + 1"

definition "size_of_term t = add_size_term t 0"

fun add_size_type :: "typ \<Rightarrow> int \<Rightarrow> int" where
  "add_size_type (Type _ tys) n = fold add_size_type tys (n + 1)"
| "add_size_type _ n = n + 1"

definition "size_of_type ty = add_size_type ty 0"

fun map_atyps :: "(typ \<Rightarrow> typ) \<Rightarrow> typ \<Rightarrow> typ" where
  "map_atyps f (Type a Ts) = Type a (map (map_atyps f) Ts)"
| "map_atyps f T = f T"

lemma "map_atyps id ty = ty"
  by (induction rule: typ.induct) (simp_all add: map_idI)

fun map_aterms :: "(term \<Rightarrow> term) \<Rightarrow> term \<Rightarrow> term" where
  "map_aterms f (t $ u) = map_aterms f t $ map_aterms f u"
| "map_aterms f (Abs a T t) = Abs a T (map_aterms f t)"
| "map_aterms f t = f t"

lemma "map_aterms id t = t"
  by (induction rule: term.induct) simp_all

definition "map_type_tvar f = map_atyps (\<lambda>x . case x of TVar iname s \<Rightarrow>  f iname s | T \<Rightarrow> T)"
definition "map_type_tfree f = map_atyps (\<lambda>x . case x of TFree name s \<Rightarrow> f x | T \<Rightarrow> T)"

fun map_types :: "(typ \<Rightarrow> typ) \<Rightarrow> term \<Rightarrow> term" where
  "map_types f (Const a T) = Const a (f T)"
| "map_types f (Free a T) = Free a (f T)"
| "map_types f (Var v T) = Var v (f T)"
| "map_types f (Bound i) = Bound i"
| "map_types f (t $ u) = map_types f t $ map_types f u"

(* fold types and terms *)
                                  
fun fold_atyps :: "(typ \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> typ \<Rightarrow> 'a \<Rightarrow> 'a" where
  "fold_atyps f (Type _ Ts) s = fold (fold_atyps f) Ts s"
| "fold_atyps f T s = f T s"

definition "fold_atyps_sorts f =
  fold_atyps (\<lambda>x . case x of TFree n S \<Rightarrow> f (TFree n S) S | TVar vn S \<Rightarrow> f (TVar vn S) S)"

fun fold_aterms :: "(term \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> term \<Rightarrow> 'a \<Rightarrow> 'a" where
  "fold_aterms f (t $ u) s = fold_aterms f u (fold_aterms f t s)"
| "fold_aterms f (Abs _ _ t) s = fold_aterms f t s"
| "fold_aterms f a s = f a s"

fun fold_term_types :: "(term \<Rightarrow> typ \<Rightarrow> 'a \<Rightarrow> 'a) \<Rightarrow> term \<Rightarrow> 'a \<Rightarrow> 'a" where 
  "fold_term_types f (Const n T) s = f (Const n T) T s"
| "fold_term_types f (Free n T) s = f (Free n T) T s"
| "fold_term_types f (Var idn T) s = f (Var idn T) T s"
| "fold_term_types f (Bound _) s = s"
| "fold_term_types f (Abs n T b) s = fold_term_types f b (f (Abs n T b) T s)"
| "fold_term_types f (t $ u) s = fold_term_types f u (fold_term_types f t s)"

definition "fold_types f = fold_term_types (\<lambda>x . f)"

(* Patterns for empty list except with Bound missing. Probably need a precond when using *)
fun replace_types :: "term \<Rightarrow> typ list \<Rightarrow> term \<times> typ list" where
  "replace_types (Const c _) (T # Ts) = (Const c T, Ts)"
| "replace_types (Free x _) (T # Ts) = (Free x T, Ts)"
| "replace_types (Var xi _) (T # Ts) = (Var xi T, Ts)"
| "replace_types (Bound i) Ts = (Bound i, Ts)"
| "replace_types (Abs x _ b) (T # Ts) =
    (let (b', Ts') = replace_types b Ts
    in (Abs x T b', Ts'))"
| "replace_types (t $ u) Ts =
    (let
      (t', Ts') = replace_types t Ts in
      (let (u', Ts'') = replace_types u Ts
    in (t' $ u', Ts'')))"

(* collect variables, is the order important? Or should I just return sets? *)
definition "add_tvar_namesT = fold_atyps (\<lambda>x . case x of TVar xi _  => insert xi | _ => id)"
definition "add_tvar_names = fold_types add_tvar_namesT"
definition "add_tvarsT = fold_atyps (\<lambda>x . case x of TVar idn s => insert (idn,s) | _ => id)"
definition "add_tvars = fold_types add_tvarsT"
definition "add_var_names = fold_aterms (\<lambda>x . case x of Var xi _ => insert xi | _ => id)"
definition "add_vars = fold_aterms (\<lambda>x . case x of Var idn s => insert (idn,s) | _ => id)"
definition "add_tfree_namesT = fold_atyps (\<lambda>x . case x of TFree a _ => insert a | _ => id)"
definition "add_tfree_names = fold_types add_tfree_namesT"
definition "add_tfreesT = fold_atyps (\<lambda>x . case x of TFree n s => insert (n,s) | _ => id)"
definition "add_tfrees = fold_types add_tfreesT"
definition "add_free_names = fold_aterms (\<lambda>x . case x of Free x _ => insert x | _ => id)"
definition "add_frees = fold_aterms (\<lambda>x . case x of Free n s => insert (n,s) | _ => id)"
definition "add_const_names = fold_aterms (\<lambda>x . case x of Const c _ => insert c | _ => id)"
definition "add_consts = fold_aterms (\<lambda>x . case x of Const n s => insert (n,s) | _ => id)"
(*which of those do I need ^ *)

(* Show that these behave like T?(Vars|Frees)T? *)

(*extra type variables in a term, not covered by its type*)
definition "hidden_polymorphism t =
  (let T = fastype_of t in
  (let tvarsT = Option.bind T (\<lambda>x. Some (add_tvarsT x empty)) in
  (let extra_tvars = Option.bind tvarsT (\<lambda>tvarsT. Some (fold_types (fold_atyps
      (\<lambda>x. case x of TVar v s \<Rightarrow> if (v,s) \<in> tvarsT then id else insert (v,s)  | _ => id)) t empty))
  in extra_tvars)))"




end