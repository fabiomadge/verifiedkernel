
theory AbstractPure
  imports Abstract
begin

(* 
  Basic version of IsabellePures core logic 
  
  Sorry, not really as final as I wanted. Also I do use generalize and instantiate in here.
  Closer to actual implementation, I need to get going on consistency proof, otherwise this is pretty
  much worthless, as it is pretty much a translation from code

  proves should imply term_ok, which saves a lot of preconditions
  Still missing flex-flex pairs(But as discussed that should be mostly an extra unchanged parameter
  Still missing sort hypotheses(Internalize?)
  Hypotheses are in H(Paper call them context)
  Probably adapt to have another rule that allows refering to previously proved thm(or just stuff them into axioms)
  Lastly, more checks for wellformedness of entities probably needed. 
  Theory should be unchanged, so checks can be outside of pred
*)
abbreviation "Variables_Set S \<equiv> \<Union>s\<in>S . Variables s"
abbreviation "Frees_Set S \<equiv> \<Union>s\<in>S . Frees s"
abbreviation "Vars_Set S \<equiv> \<Union>s\<in>S . Vars s"
abbreviation "TFrees_Set S \<equiv> \<Union>s\<in>S . TFrees s"
abbreviation "TVars_Set S \<equiv> \<Union>s\<in>S . TVars s"
abbreviation "no_vars A \<equiv> Vars A = {} \<and> TVars A = {}"

consts generalize :: "name set \<Rightarrow> name set \<Rightarrow> term \<Rightarrow> term"
consts generalize_idx :: "(name \<times> indexname) set \<Rightarrow> (name \<times> indexname) set \<Rightarrow> term \<Rightarrow> term"
consts instantiate :: "((indexname \<times> sort) \<times> typ) set \<Rightarrow> ((indexname \<times> typ) \<times> term) set \<Rightarrow> term \<Rightarrow> term"
inductive proves :: "theory \<Rightarrow> term set \<Rightarrow> term \<Rightarrow> bool" ("(_,_) \<turnstile> (_)" 50) for \<Theta> where
  axiom: "A\<in>axioms_of \<Theta> \<Longrightarrow> \<Theta>, {} \<turnstile> A"
| "assume": "term_ok \<Theta> A \<Longrightarrow> no_vars A \<Longrightarrow> \<Theta>,{A} \<turnstile> A"
(*Is term_ok really necessary here? Shouldn't this be ensured by construction? \<Longrightarrow> Corollary*)
| forall_intro_var: "\<Theta>, \<Gamma> \<turnstile> B \<Longrightarrow> (x,\<tau>)\<in>Vars B \<Longrightarrow> term_ok \<Theta> B
  \<Longrightarrow> \<Theta>, \<Gamma> \<turnstile> mk_all' x \<tau> B"
| forall_intro_free: "\<Theta>, \<Gamma> \<turnstile> B \<Longrightarrow> (x,\<tau>)\<in>Frees B \<Longrightarrow> (x,\<tau>)\<notin>Frees_Set \<Gamma> \<Longrightarrow> term_ok \<Theta> B
  \<Longrightarrow> \<Theta>, \<Gamma> \<turnstile> mk_all x \<tau> B"
| forall_elim: "\<Theta>, \<Gamma> \<turnstile> Const ''Pure.all'' ((\<tau> \<rightarrow> _) \<rightarrow> _) $ B
  \<Longrightarrow> term_ok \<Theta> a \<Longrightarrow> typ_of a = Some \<tau>
  \<Longrightarrow> \<Theta>, \<Gamma> \<turnstile> B \<bullet> a"
| implies_intro: "\<Theta>, \<Gamma> \<turnstile> B \<Longrightarrow> \<Theta>, \<Gamma> - {A} \<turnstile> A \<longmapsto> B"
| implies_elim: "\<Theta>, \<Gamma>\<^sub>1 \<turnstile> A \<longmapsto> B \<Longrightarrow> \<Theta>, \<Gamma>\<^sub>2 \<turnstile> A \<Longrightarrow> \<Theta>, \<Gamma>\<^sub>1\<union>\<Gamma>\<^sub>2 \<turnstile> B"
(*Not done. Needs many preconditions. May make sense to do simultaneously.*)
| axiom': "A\<in>axioms_of \<Theta> \<Longrightarrow> \<Theta>, {} \<turnstile> instantiate instTs insts (generalize_idx tfreesM freesM A)"

  (* Combine those rules probably *)
(*| generalizeTyp: "proves thy H B \<Longrightarrow> \<alpha> \<notin> TFrees_Set H \<Longrightarrow> proves thy H (generalize [\<alpha>] [] B)"
| generalizeTerm: "proves thy H B \<Longrightarrow> x \<notin> Frees_Set H \<Longrightarrow> proves thy H (generalize [] [x] B)"
| instantiateTyp: "proves thy H B \<Longrightarrow> typ_ok (signature_of thy) \<tau> \<Longrightarrow> \<alpha> \<in> TVars_Set H
  \<Longrightarrow> proves thy H (instantiate [(\<alpha>,\<tau>)] [] B)"
| instantiateTerm: "proves thy H B \<Longrightarrow> term_ok (signature_of thy) t \<Longrightarrow> x \<in> Vars_Set H
  \<Longrightarrow> proves thy H (instantiate [] [(x,t)] B)"*)
  (* Missing: Stuff for typeclasses *)

(*Can we do this without equality?*)
  (*
    I do not know how to axomatise Thm.beta_conversion, in the proof terms we do not record it
    Only one step, full beta conversion by applying this rule multiple times
  *)
| \<beta>_conversion: "\<Theta>, \<Gamma> \<turnstile> ((Abs s T t) $ x) \<Longrightarrow> \<Theta>, \<Gamma> \<turnstile> (mk_eq ((Abs s T t) $ x) (subst_bound x t))"


(* Version where the new indexname is supplied lifted to arbitrary theorems *)
lemma generalize_idx:
  assumes "\<Theta>, \<Gamma> \<turnstile> B"
    "fst ` tfreesM \<union> fst ` TFrees_Set \<Gamma> = {}" "snd ` tfreesM \<union> fst ` Vars B = {}"
    "fst ` freesM \<union> fst ` TFrees_Set \<Gamma> = {}" "snd ` freesM \<union> fst ` Vars B = {}"
  shows "\<Theta>, \<Gamma> \<turnstile> generalize_idx tfreesM freesM B"
  sorry

(*
  Not trivial, because the new idn depends on the maxidx (not part of the abstract theorem
  representation) of the thm in the implementation.
    \<Longrightarrow> Probably not possible here.
*)
lemma generalize:
  assumes "\<Theta>, \<Gamma> \<turnstile> B"
    "tfrees \<union> fst ` TFrees_Set \<Gamma> = {}"
    "frees \<union> fst ` TFrees_Set \<Gamma> = {}"
  shows "\<Theta>, \<Gamma> \<turnstile> generalize tfrees frees B"
  sorry

lemma instantiate:
  assumes "\<Theta>, \<Gamma> \<turnstile> B"
    "TVars B \<subseteq> fst ` instTs"
    (* Is this both strong enough and sufficient? Most likely not. (of_sort \<tau> S?) *)
    "\<forall>((\<alpha>,S),\<tau>)\<in>instTs. typ_ok \<Theta> \<tau> \<and> of_sort (sorts_of (signature_of \<Theta>)) \<tau> S"

    "Vars B \<subseteq> fst ` insts"
    "\<forall>((x,T),t)\<in>insts. term_ok \<Theta> t \<and> Some T = typ_of t"
  shows "\<Theta>, \<Gamma> \<turnstile> instantiate instTs insts B"
  sorry



(* Axiomatise Equality here, state what axioms and definitions need to be in theory *)



(* Problem: This works to easily *) (*Really?*)
lemma trivial: "term_ok \<Theta> c \<Longrightarrow> no_vars c \<Longrightarrow> \<Theta>, {c} \<turnstile> c"
  by (rule "assume") simp

lemma weaken_proves: "\<Theta>, \<Gamma> \<turnstile> c \<Longrightarrow> \<Gamma>\<subseteq>\<Gamma>' \<Longrightarrow> \<Theta>, \<Gamma>' \<turnstile> c" 
  apply (induction \<Gamma> c arbitrary: \<Gamma>' rule: proves.induct)
          apply (auto intro: proves.intros)
  sorry
  (*subgoal for H B x ty s H'
    apply (rule forall_intro[of thy H' B x])
    apply assumption
       apply assumption
      defer
    apply assumption
     apply assumption thm proves.induct[of thy]
    sorry (* Doesnt' work, Need to make sure no new variables are captured by hyps *)
  oops*)
end