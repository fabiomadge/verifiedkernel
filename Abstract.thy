
(* 
  A small, abstract version of the Isabelle/Pure deduction system and the one used in the below paper.
  In this I ignore abbreviations and regard sorts as part of the names
  I want to use the one from the paper for the consistency proof and then show it being equivalent
  to the minimal one for IsabellePure. This version can then be used check the proof checker agains
  or as a basis for verifying Pure.

  The _ok functions should be the same for both, differences in signature content and proves predicate

  Based on: A Mechanised Semantics for HOL with Ad-hoc Overloading
*)

theory Abstract
  imports Term Sorts
begin


(* No occurs check here?, from paper *)
definition "tinstT T1 T2 \<equiv> \<exists>\<rho>. tsubstT' T2 \<rho> = T1"
definition "tinst t1 t2 \<equiv> \<exists>\<rho>. tsubst' t2 \<rho> = t1"
definition "inst t1 t2 \<equiv> \<exists>\<rho>. subst' t2 \<rho> = t1" 

lemma "tinstT (TVar (''a'', 0) {} \<rightarrow> TVar (''b'', 0) {}) (TVar (''c'', 0) {})"
  unfolding tinstT_def by auto


datatype signature = Signature
  (const_typ_of: "string \<Rightarrow> typ option")
  (typ_arity_of: "string \<Rightarrow> nat option")
  (sorts_of: "algebra")

(* analogous to certify operations *)
fun typ_ok_sig :: "signature \<Rightarrow> typ \<Rightarrow> bool" where
  "typ_ok_sig sig (Type c Ts) = (case typ_arity_of sig c of
    None \<Rightarrow> False
  | Some ar \<Rightarrow> length Ts = ar \<and> list_all (typ_ok_sig sig) Ts)"
| "typ_ok_sig _ _ = True"




fun term_ok' :: "signature \<Rightarrow> term \<Rightarrow> bool" where
  "term_ok' sig (Var _ T) = typ_ok_sig sig T"
| "term_ok' sig (Free _ T) = typ_ok_sig sig T"
| "term_ok' sig (Bound _) = True"
| "term_ok' sig (Const s T) = (case const_typ_of sig s of
    None \<Rightarrow> False
  | Some ty \<Rightarrow> typ_ok_sig sig T \<and> tinstT T ty)"
| "term_ok' sig (f $ u) \<longleftrightarrow> term_ok' sig f \<and> term_ok' sig u" (* HOL4 has a typecheck here, moved to top because of bounds *)
| "term_ok' sig (Abs s T t) \<longleftrightarrow> typ_ok_sig sig T \<and> term_ok' sig t"

datatype "theory" = Theory (signature_of: signature) (axioms_of: "term set")

definition "typ_ok thy T \<equiv> typ_ok_sig (signature_of thy) T"
definition "term_ok thy t \<equiv> term_ok' (signature_of thy) t \<and> typ_of t \<noteq> None"

(* fun for easy pattern matching *)
fun is_std_sig where "is_std_sig (Signature const_typ typ_arity sorts) \<longleftrightarrow>
    typ_arity ''fun'' = Some 2 \<and> typ_arity ''prop'' = Some 0 
  \<and> const_typ ''Pure.eq'' = Some ((TVar (''a'', 0) {}) \<rightarrow> (TVar (''a'', 0) {}) \<rightarrow> propT)"

fun theory_ok where "theory_ok (Theory (Signature const_typ typ_arity sorts) axioms) \<longleftrightarrow>
    (\<forall>ty \<in> Map.ran const_typ . typ_ok_sig (Signature const_typ typ_arity sorts) ty)
  \<and> (\<forall>p \<in> axioms . term_ok' (Signature const_typ typ_arity sorts) p \<and> typ_of p = Some propT)
  \<and> is_std_sig (Signature const_typ typ_arity sorts)"

definition "minimal_std_sig = Signature (Map.empty(''Pure.eq''\<mapsto>(TVar (''a'', 0) {}) \<rightarrow> (TVar (''a'', 0) {}) \<rightarrow> propT))
   (Map.empty(''fun''\<mapsto>2)(''prop''\<mapsto>0)) empty_algebra"
lemma minimal_std_sig_is_std_sig: "is_std_sig minimal_std_sig"
  by (simp add: minimal_std_sig_def)

lemma minimal_theory_ok: "theory_ok (Theory minimal_std_sig empty)"
  unfolding minimal_std_sig_def by simp

(* 
  Potential problem: I do union with equality instead of aconv, 
  this may lead to some assumptions appearing multiple times
  
  Should I move the theory_ok, etc checks into each rule?
*)
fun is_tvariable :: "typ \<Rightarrow> bool" where
  "is_tvariable (Type _ _) = False" 
| "is_tvariable _ = True"

definition "fun_of list = fold (\<lambda>(x,y) f. f(x:=y)) list id"

inductive proves :: "theory \<Rightarrow> term set \<Rightarrow> term \<Rightarrow> bool" for thy where
  ABS: "proves thy ctxt (mk_eq l r) \<Longrightarrow> v \<notin> (\<Union>h\<in>ctxt . Variables h) \<Longrightarrow>
    proves thy ctxt (mk_eq (mk_Abstraction s v l) (mk_Abstraction s v r))"
| ASSUME: "theory_ok thy \<Longrightarrow> typ_of p = Some propT \<Longrightarrow> term_ok thy p
    \<Longrightarrow> proves thy {p} p"
| BETA: "theory_ok thy \<Longrightarrow> term_ok thy v \<Longrightarrow> term_ok thy t
    \<Longrightarrow> proves thy {} (mk_eq ((mk_Abstraction s v t) $ v) t)"
| DEDUCT_ANTISYM: "proves thy h1 c1 \<Longrightarrow> proves thy h2 c2 
    \<Longrightarrow> proves thy (h1-{c2}\<union>h2-{c1}) (mk_eq c1 c2)"
| EQ_MP: "proves thy h1 (mk_eq p q) \<Longrightarrow> proves thy h2 p' \<Longrightarrow> aconv p p' \<Longrightarrow> proves thy (h1\<union>h2) q"
  (* I'd like to use the abstract version here, both instantiate rules suck *)
| INST: "(\<forall>s s'. (s,s') \<in> set insts 
    \<longrightarrow> is_variable s \<and> (\<exists>ty. typ_of s = Some ty \<and> typ_of s' = Some ty \<and> term_ok thy s'))
  \<Longrightarrow> proves thy h c \<Longrightarrow> proves thy (subst_free insts ` h) (subst_free insts c)"
  (* This is reasonably abstract, probably extract wellformedness criteria for insts list *)
| INST_TYPE: "(\<forall>s s' . (s,s') \<in> set tinsts \<longrightarrow> is_tvariable s \<and> typ_ok thy s')
  \<Longrightarrow> proves thy ((\<lambda>h. tsubst' h (fun_of tinsts)) ` h) (tsubst' c (fun_of tinsts))"
| MK_COMB: "proves thy h1 (mk_eq l1 r1) \<Longrightarrow> proves thy h2 (mk_eq l2 r2) 
  \<Longrightarrow> proves thy (h1\<union>h2) (mk_eq (l1 $ l2) (r1 $ r2))"
| REFL: "theory_ok thy \<Longrightarrow> term_ok thy t \<Longrightarrow> proves thy {} (mk_eq t t)"
| AXIOM: "theory_ok thy \<Longrightarrow> c \<in> axioms_of thy \<Longrightarrow> proves thy {} c"


lemma "\<Theta> = Theory minimal_std_sig empty \<Longrightarrow> term_ok \<Theta> t \<Longrightarrow> proves \<Theta> empty (mk_eq t t)"
  using minimal_theory_ok REFL by simp

end