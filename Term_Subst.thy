
(*
  For stuff from Term_Subst.ML
  Debatting to also moving my abstract substitutions here and showing that all types do the same
  for their respective scopes. That would also make the build smoother ;

  Most importantly: I do not use references and SAME structure in the formalization. 
  The references are seemingly only used to collect the maxidx of terms, types. For formalization
  just run additional pass of maxidx collection function.
  For SAME use options instead

*)

theory Term_Subst
  imports Term
begin

fun generalizeT :: "name list \<Rightarrow> int \<Rightarrow> typ \<Rightarrow> typ" where
  "generalizeT [] _ ty = ty"
| "generalizeT tfrees idx (Type a Ts) = Type a (map (generalizeT tfrees idx) Ts)"
| "generalizeT tfrees idx (TFree a S) = (if a\<in>set tfrees then TVar (a,idx) S else TFree a S)"
| "generalizeT _ _ (TVar a S) = TVar a S"

lemma generalizeT_tfrees_not_occuring: 
  "set tfrees \<inter> fst ` (TFreesT ty) = {} \<Longrightarrow> generalizeT tfrees i ty = ty"
  apply (induction tfrees i ty rule: generalizeT.induct) 
  apply auto
  apply (simp add: image_UN inf_set_def map_idI)
  done

fun generalize :: "name list \<Rightarrow> name list \<Rightarrow> int \<Rightarrow> term \<Rightarrow> term" where
  "generalize [] [] _ t = t"
| "generalize tfrees frees idx (Free x T) = (if x \<in> set frees 
    then Var (x, idx) (generalizeT tfrees idx T) 
    else Free x (generalizeT tfrees idx T))"
| "generalize tfrees frees idx (Var xi T) = Var xi (generalizeT tfrees idx T)"
| "generalize tfrees frees idx (Const c T) = Const c (generalizeT tfrees idx T)"
| "generalize tfrees frees idx (Bound n) = Bound n"
| "generalize tfrees frees idx (Abs x T t) = 
    Abs x (generalizeT tfrees idx T) (generalize tfrees frees idx t)"
| "generalize tfrees frees idx (t $ u) = 
    generalize tfrees frees idx t $ generalize tfrees frees idx u"

lemma generalize_tfrees_frees_not_occuring:
  "set tfrees \<inter> fst ` TFrees t = {} \<Longrightarrow> set frees \<inter> fst ` Frees t = {} 
  \<Longrightarrow> generalize tfrees frees i t = t"
  by (induction tfrees frees i t rule: generalize.induct) 
    (auto simp add: generalizeT_tfrees_not_occuring image_Un inf_sup_distrib1)

(* No real lemmas about the generalize functions yet :( *)

(* Abusing fun for easy pattern matching *)
fun no_index where "no_index (x, y) = (x, (y, -1))"
fun no_indexes1 where "no_indexes1 inst1 = map no_index inst1"
fun no_indexes2 where "no_indexes2 (inst1, inst2) = (map no_index inst1, map no_index inst2)"

fun subst_typ :: "((indexname * sort) * typ) list \<Rightarrow> typ \<Rightarrow> typ" where
  "subst_typ inst (Type a Ts) = 
    Type a (map (subst_typ inst) Ts)"
| "subst_typ inst (TVar idn S) = the_default (TVar idn S) 
    (lookup (\<lambda>x . x = (idn, S)) inst)"
| "subst_typ _ T = T"

lemma subst_typ_empty_no_change: "subst_typ [] T = T"
  by (induction T) (auto simp add: map_idI)

(* Move to Term.thy *)
lemma finite_TVarsT: "finite (TVarsT T)"
  by (induction T) auto
lemma no_TVarsT_imp_tsubsT_unchanged: "TVarsT T = {} \<Longrightarrow> tsubstT T \<rho> = T"
  by (induction T) (auto simp add: map_idI)
 
lemma subst_typ_irrelevant_order:
  assumes "distinct (map fst pairs)" and "distinct (map fst pairs')" and "set pairs = set pairs'"
shows "subst_typ pairs T = subst_typ pairs' T"
  using assms
proof(induction T)
  case (Type n Ts)
  then show ?case by (induction Ts) auto
next
  case (TVar idn S)
  then show ?case apply auto using lookup_eq_order_irrelevant
    by (metis (full_types))
next
  case (TFree n S)
  then show ?case by simp
qed

(* Core lemma, Isabelle/Pure's instantiateT_same function can simulate abstract type subtitutions 
  in types *)

(* proof is much nicer. YAY Apply idea to other proofs to *)
lemma subst_typ_simulates_tsubstT_gen':"distinct l \<Longrightarrow> TVarsT T \<subseteq> set l 
  \<Longrightarrow> tsubstT T \<rho>  = subst_typ (map (\<lambda>(x,y).((x,y), \<rho> x y)) l) T"
proof(induction T arbitrary: l)
  case (Type n Ts)
  then show ?case by (induction Ts) auto
next
  case (TVar idn S)
  hence d: "distinct (map fst (map (\<lambda>(x,y).((x,y), \<rho> x y)) l))" 
    by (simp add: case_prod_beta map_idI)
  hence el: "((idn,S), \<rho> idn S) \<in> set (map (\<lambda>a. case a of (x, y) \<Rightarrow> ((x, y), \<rho> x y)) l)" 
    using TVar by auto
  show ?case using lookup_present_eq_key[OF _ el] TVar.prems d by auto
next
  case (TFree n S)
  then show ?case by simp
qed

lemma subst_typ_simulates_tsubstT_gen: "tsubstT T \<rho> 
  = subst_typ (map (\<lambda>(x,y).((x,y), \<rho> x y)) (SOME l . distinct l \<and> TVarsT T \<subseteq> set l)) T"
proof(rule someI2_ex)
  show "\<exists>a. distinct a \<and> TVarsT T \<subseteq> set a"
    using finite_TVarsT finite_distinct_list
    by (metis order_refl)
next
  fix l assume l: "distinct l \<and> TVarsT T \<subseteq> set l"
  
  then show "tsubstT T \<rho> = subst_typ (map (\<lambda>a. case a of (x, y) \<Rightarrow> ((x, y), \<rho> x y)) l) T"
    using subst_typ_simulates_tsubstT_gen' by blast
qed


corollary subst_typ_simulates_tsubstT: "tsubstT T \<rho> 
  = subst_typ (map (\<lambda>(x,y).((x,y), \<rho> x y)) (SOME l . distinct l \<and> set l = TVarsT T)) T"
  apply (rule someI2_ex) using finite_TVarsT finite_distinct_list apply metis
  using subst_typ_simulates_tsubstT_gen' by simp

(* Other direction, can construct a abstract substitution for one performed by instantiateT_same *)
lemma "subst_typ inst T
  = tsubstT T (\<lambda>idn S . the_default (TVar idn S) (lookup (\<lambda>x. x=(idn, S)) inst))"
  by (induction T) auto

fun subst_term :: "((indexname * sort) * typ) list \<Rightarrow> ((indexname * typ) * term) list \<Rightarrow> term \<Rightarrow> term" where
  "subst_term instT inst (Const c T) = Const c (subst_typ instT T)"
| "subst_term instT inst (Free x T) = Free x (subst_typ instT T)"
| "subst_term instT inst (Var idn T) = (let T' = subst_typ instT T in
  the_default (Var idn T') (lookup (\<lambda>x. x=(idn, T')) inst))"
| "subst_term _ _ (Bound n) = Bound n"
| "subst_term instT inst (Abs x T t) = Abs x (subst_typ instT T) (subst_term instT inst t)"
| "subst_term instT inst (t $ u) = subst_term instT inst t $ subst_term instT inst u"

lemma subst_term_empty_no_change: "subst_term [] [] t = t"
  by (induction t) (simp_all add: subst_typ_empty_no_change)

(* Move to Term.thy *)
lemma finite_Vars: "finite (Vars t)"
  by (induction t) auto
lemma finite_TVars: "finite (TVars t)"
  by (induction t) (auto simp add: finite_TVarsT)
lemma no_TVars_imp_tsubst_unchanged: "TVars t = {} \<Longrightarrow> tsubst t \<rho> = t"
  by (induction t) (auto simp add: map_idI no_TVarsT_imp_tsubsT_unchanged) 
lemma no_Vars_imp_subst_unchanged: "Vars t = {} \<Longrightarrow> subst t \<rho> = t"
  by (induction t) (auto simp add: map_idI)
 
lemma subst_term_irrelevant_order:
  assumes instT_assms: "distinct (map fst instT)" "distinct (map fst instT')" "set instT = set instT'"
  assumes inst_assms: "distinct (map fst inst)" "distinct (map fst inst')" "set inst = set inst'"
shows "subst_term instT inst t = subst_term instT' inst' t"
  using assms
proof(induction t)
  case (Var idn T)
  then show ?case  apply (simp add: Let_def subst_typ_irrelevant_order[OF Var.prems(1-3)])
    using lookup_eq_order_irrelevant
    by (metis Var.prems(4) Var.prems(5) inst_assms)
next
  case (Abs x T t)
  then show ?case using subst_typ_irrelevant_order[OF instT_assms] by simp
qed (simp_all add: subst_typ_irrelevant_order[OF instT_assms])

(* Core lemma, Isabelle/Pure's instantiate_same function can simulate abstract 
  term/type subtitutions in terms 

  The tsubst should be no problem, can be rewritten to subst_type using previous simulation lemma
*)
lemma subst_term_simulates_subst_tsubst_gen':
  assumes lty_assms: "distinct lty" "TVars t \<subseteq> set lty"
  assumes lt_assms: "distinct lt" "Vars (tsubst t \<rho>ty) \<subseteq> set lt"
  shows "subst (tsubst t \<rho>ty) \<rho>t 
    = subst_term (map (\<lambda>(x,y).((x,y), \<rho>ty x y)) lty) (map (\<lambda>(x,y).((x,y), \<rho>t x y)) lt) t"
proof-
  let ?lty = "map (\<lambda>(x,y).((x,y), \<rho>ty x y)) lty"

  have p1ty: "distinct (map fst ?lty)" using lty_assms
    by (simp add: case_prod_beta map_idI)

  let ?lt = "map (\<lambda>(x,y).((x,y), \<rho>t x y)) lt"

  have p1t: "distinct (map fst ?lt)" using lt_assms
    by (simp add: case_prod_beta map_idI)

  show ?thesis using assms
  proof(induction t arbitrary: lty lt)
    case (Var idn T)
  
    let ?T = "tsubstT T \<rho>ty"  
    have el: "((idn, ?T), \<rho>t idn ?T) \<in> set (map (\<lambda>(x,y).((x,y), \<rho>t x y)) lt)" 
      using Var by auto
    have d: "distinct (map fst (map (\<lambda>(x,y).((x,y), \<rho>t x y)) lt))" 
      using Var by (simp add: case_prod_beta map_idI)
    show ?case using  Var.prems d 
      by (auto simp add: lookup_present_eq_key[OF d el] 
          subst_typ_simulates_tsubstT_gen'[symmetric] Let_def)
  qed (simp_all add: subst_typ_simulates_tsubstT_gen')
qed

corollary subst_term_simulates_subst_tsubst: "subst (tsubst t \<rho>ty) \<rho>t 
    = subst_term (map (\<lambda>(x,y).((x,y), \<rho>ty x y)) (SOME lty . distinct lty \<and> TVars t = set lty)) 
      (map (\<lambda>(x,y).((x,y), \<rho>t x y)) (SOME lt . distinct lt \<and> Vars (tsubst t \<rho>ty) = set lt)) t"
  apply (rule someI2_ex)
  using finite_Vars finite_distinct_list apply metis
  apply (rule someI2_ex) 
  using finite_TVars finite_distinct_list apply metis
  using subst_term_simulates_subst_tsubst_gen' by simp

abbreviation "instatiateT_maxidx instT ty i \<equiv> 
  let ty' = subst_typ instT ty in (ty', maxidx_typ ty' i)"

abbreviation "instantiate_maxidx instT inst t i \<equiv>
  let t' = subst_term instT inst t in (t', maxidx_term t' i)"

(* 
  Want to show that generalize + subst are somehwat "dual". 
  Problem: Generalize functions only looks at names, subst functions also at typ or sort
*)

end