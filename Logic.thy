
theory Logic
  imports Sorts
begin


(** types as terms **)

definition "mk_type ty = Const ''Pure.type'' (Term.itselfT ty)"


(* pattern matching on strings not possible *)
fun dest_type :: "term \<Rightarrow> typ option" where
  "dest_type (Const nc (Type nt [ty])) = 
    (if nc = ''Pure.type'' \<and> nt = ''Pure.type'' then Some ty else None)"
| "dest_type t = None"

definition "type_map f t = map_option (\<lambda>ty. mk_type (f ty)) (dest_type t)"



(** type classes **)

(* const names *)

abbreviation "classN == ''_class''"

(* i have implementations for those somewhere, find them *)
consts suffix :: "string \<Rightarrow> string \<Rightarrow> string"
consts unsuffix :: "string \<Rightarrow> string \<Rightarrow> string option"
abbreviation "const_of_class == suffix classN"

abbreviation "class_of_const c == unsuffix classN c"


(* class/sort membership *)

definition "mk_of_class ty c =
  Const (const_of_class c) (Term.itselfT ty \<rightarrow> propT) $ mk_type ty"

fun dest_of_class :: "term \<Rightarrow> (typ * class) option" where 
  "dest_of_class (Const c_class _ $ ty) = lift2_option Pair (dest_type ty) (class_of_const c_class)"
| "dest_of_class _ = None"

definition "mk_of_sort ty S == map (\<lambda>c . mk_of_class ty c) S"


end