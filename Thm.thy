
theory Thm
  imports Term Sorts Type Context Sign
begin

(* Prototype ctyps, cterms, assume "flat" theories, everything but certificate could be directly
  be computed from underlying typ/terms *)

(* 
  I need to make abstract subtypes of those, containing only valid ctyp/cterms. 
  Otherwise using those definitions just increases the number of assumptions I need to carry around.
  Problem: Valid ctyps/cterms depend on theory \<rightarrow> smells like dependent types
  I define them anyway here, but suggest we just use typs/terms + invariant that the typ/term passed
  the corresponding certify operation.
*)

datatype ctyp' = Ctyp (cert: "theory") ("typ_of": "typ") (maxidx: int) (sorts: "sort set")
datatype cterm' = Cterm (cert: "theory") ("term_of": "term") ("typ": "typ") (maxidx: int) (sorts: "sort set")

definition "global_ctyp_of' thy raw_T = 
    map_option (\<lambda>T. Ctyp thy T (maxidx_of_typ T) (insert_typ T {})) (certify_typ thy raw_T)"

definition "global_cterm_of' thy raw_t = 
    map_option (\<lambda>(t,T,maxidx). Cterm thy t T maxidx (insert_term t {})) (certify_term thy raw_t)"


end