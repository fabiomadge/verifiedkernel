
theory Type
  imports Term Sorts 
  (* AFP, Currently only for mapM, see if you can use stuff from there instead of the current Preliminaries *)
  "First_Order_Terms.Option_Monad"
begin

(* Ignoring syntactic types for now, I'd also like to drop abbreviations *)
datatype decl = LogicalType nat | Abbreviation "string list" "typ"

datatype tsig = Tsig (sorts: Sorts.algebra) (types: "string \<Rightarrow> decl option")

(* Bunch more of those wrappers. Check first which ones are needed *)
definition "sort_eq tsig s1 s2 == Sorts.wf_classes_loc.sort_eqv (classes (sorts tsig)) s1 s2"
definition "subsort tsig s1 s2 == Sorts.wf_classes_loc.sort_leq (classes (sorts tsig)) s1 s2"

(* Use booleans instead of exceptions in contrast to ML *)
definition "cert_class' tsig c == c \<in> Field (classes (sorts tsig))"
definition "cert_sort' tsig s == \<forall>c\<in>s . cert_class' tsig c"

definition "cert_sort tsig s \<equiv> if cert_sort' tsig s then Some s else None"

fun inst_typ :: "(string \<times> typ) list \<Rightarrow> typ \<Rightarrow> typ" where
  "inst_typ env (Type c Ts) = Type c (map (inst_typ env) Ts)"
  (* Probably define a "get_first" function for the map_option find abomination *)
| "inst_typ env (TFree x s) = the_default (TFree x s) (map_option snd (List.find (\<lambda>(y,_). y=x) env))"
| "inst_typ _ T = T"

(* 
  Drop mode for now, I hope we just need the default mode
  I should probably go directly for do notation by now...
 *)
fun cert_typ :: "tsig \<Rightarrow> typ \<Rightarrow> typ option" where
  "cert_typ tsig (Type c Ts) = (case types tsig c of
      Some (LogicalType n) \<Rightarrow> 
        if length Ts \<noteq> n then None else map_option (Type c) (mapM (cert_typ tsig) Ts)
    | Some (Abbreviation vs U) \<Rightarrow> if length Ts \<noteq> length vs then None else 
        map_option (\<lambda>Ts' . inst_typ (zip vs Ts') U) (mapM (cert_typ tsig) Ts)
    | None \<Rightarrow> None)"
| "cert_typ tsig (TFree x S) = map_option (TFree x) (cert_sort tsig S)"
| "cert_typ tsig (TVar (x,i) S) = (if i<0 then None else map_option (TVar (x,i)) (cert_sort tsig S))"


fun varify_global :: "(string \<times> sort) list \<Rightarrow> term \<Rightarrow> ((string \<times> sort) \<times> indexname) list \<times> term" where
  "varify_global fixed t = undefined"
                  
fun of_sort :: "tsig \<Rightarrow> typ \<Rightarrow> sort \<Rightarrow> bool" where
  "of_sort tsig ty S = Sorts.of_sort (sorts tsig) ty S"

end