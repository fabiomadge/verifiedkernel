
(* Useful stuff, so far mostly to replace exceptions *)

theory Preliminaries
  imports Main 
begin

sledgehammer_params [provers = cvc4 z3 spass e remote_vampire]

(* Are there any helpful lemmas about this? *)
fun the_default :: "'a \<Rightarrow> 'a option \<Rightarrow> 'a" where
  "the_default a None = a"
| "the_default _ (Some b) = b"

abbreviation Or :: "'a option \<Rightarrow> 'a option \<Rightarrow> 'a option" (infixl "OR" 60) where
  "e1 OR e2 \<equiv> case e1 of None \<Rightarrow> e2 | p \<Rightarrow> p"

lemma Or_Some: "(e1 OR e2) = Some x \<longleftrightarrow> e1 = Some x \<or> (e1 = None \<and> e2 = Some x)"
  by(auto split: option.split)

lemma Or_None: "(e1 OR e2) = None \<longleftrightarrow> e1 = None \<and> e2 = None"
  by(auto split: option.split)

fun lift2_option :: "('a \<Rightarrow> 'b \<Rightarrow> 'c) \<Rightarrow> 'a option \<Rightarrow> 'b option \<Rightarrow> 'c option" where
  "lift2_option _ None _ = None" |
  "lift2_option _ _ None = None" |
  "lift2_option f (Some x) (Some y) = Some (f x y)"

lemma lift2_option_not_None: "lift2_option f x y \<noteq> None \<longleftrightarrow> (x \<noteq> None \<and> y \<noteq> None)" 
  using lift2_option.elims by blast
lemma lift2_option_None: "lift2_option f x y = None \<longleftrightarrow> (x = None \<or> y = None)" 
  using lift2_option.elims by blast

fun find :: "('a \<Rightarrow> 'b option) \<Rightarrow> 'a list \<Rightarrow> 'b option" where
"find f [] = None" |
"find f (x#xs) = f x OR find f xs"

lemma findD:
  "find f xs = Some p \<Longrightarrow> \<exists>x \<in> set xs. f x = Some p"
  by(induction xs arbitrary: p) (auto split: option.splits)

lemma find_None:
  "find f xs = None \<longleftrightarrow> (\<forall>x \<in> set xs. f x = None)"
  by(induction xs) (auto split: option.splits)

lemma find_ListFind: "find f l = Option.bind (List.find (\<lambda>x. case f x of None \<Rightarrow> False | _ \<Rightarrow> True) l) f" 
  by (induction l) (auto split: option.split)
     
lemma "List.find P l = Some p \<Longrightarrow> \<exists>p \<in> set l . P p"
  by (induction l) (auto split: if_splits)

lemma find_the_pair:
  assumes "distinct (map fst pairs)" 
    and "\<And>x y. x\<in>set (map fst pairs) \<Longrightarrow> y\<in>set (map fst pairs) \<Longrightarrow> P x \<Longrightarrow> P y \<Longrightarrow> x = y"
    and "(x,y) \<in> set pairs" and "P x" 
  shows "List.find (\<lambda>(x,_) . P x) pairs = Some (x,y)"
  using assms
proof (induction pairs)
  case Nil
  then show ?case by simp
next
  case (Cons a pairs)
  show ?case apply auto 
    using Cons apply (metis list.set_intros(1) set_zip_leftD zip_map_fst_snd)
    using Cons apply (metis distinct.simps(2) distinct_map list.set_intros(1) list.simps(9) map_of_SomeD map_of_is_SomeI prod.inject set_ConsD set_zip_leftD zip_map_fst_snd)
    using Cons by (metis (no_types, lifting) distinct.simps(2) fst_conv list.set_intros(2) list.simps(9) set_ConsD)
qed

lemma find_pairs_order_irrel:
  assumes "distinct (map fst pairs)" "distinct (map fst pairs')" "set pairs = set pairs'"
    and "\<And>x y. x\<in>set (map fst pairs) \<Longrightarrow> y\<in>set (map fst pairs) \<Longrightarrow> P x \<Longrightarrow> P y \<Longrightarrow> x = y"
  shows "List.find (\<lambda>(x,_) . P x) pairs = List.find (\<lambda>(x,_) . P x) pairs'"
proof(cases "List.find (\<lambda>(x,_) . P x) pairs")
  case None
  then show ?thesis using assms by (metis find_None_iff)
next
  case (Some a) 
  then show ?thesis using assms find_the_pair find_None_iff 
    by (smt case_prodE case_prod_beta find_cong set_map) (* horribly slow check again *)
qed


fun remdups_on :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "remdups_on  _ [] = []" 
| "remdups_on compare (x # xs) = 
    (if \<exists>x' \<in> set xs . compare x x' then remdups_on compare xs else x # remdups_on compare xs)"

fun distinct_on :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> bool" where
  "distinct_on _ [] \<longleftrightarrow> True"
| "distinct_on compare (x # xs) \<longleftrightarrow> \<not>(\<exists>x' \<in> set xs . compare x x') \<and> distinct_on compare xs"

lemma "remdups_on (=) xs = remdups xs"
  by (induction xs) auto

lemma remdups_on_antimono:
  "(\<And>x y . f x y \<Longrightarrow> g x y) \<Longrightarrow> set (remdups_on g xs) \<subseteq> set (remdups_on f xs)"
  by (induction xs) auto

lemma remdups_on_subset_input: "set (remdups_on f xs) \<subseteq> set xs"
  by (induction xs) auto

lemma distinct_on_remdups_on: "distinct_on f (remdups_on f xs)" 
  apply (induction xs) 
   apply simp
  using remdups_on_subset_input by fastforce

lemma distinct_on_no_compare: "(\<And>x y . f x y \<Longrightarrow> f y x)\<Longrightarrow> 
  distinct_on f xs \<Longrightarrow> x\<in>set xs \<Longrightarrow> y\<in>set xs \<Longrightarrow> x\<noteq>y \<Longrightarrow> \<not> f x y"
  by (induction xs) auto

fun lookup :: "('a \<Rightarrow> bool) \<Rightarrow> ('a \<times> 'b) list \<Rightarrow> 'b option" where
  "lookup _ [] = None"
| "lookup f ((x,y)#xs) = (if f x then Some y else lookup f xs)"

(* Move to Preliminaries.thy *)
lemma lookup_present_eq_key: "distinct (map fst al) \<Longrightarrow> (k, v) \<in> set al \<Longrightarrow> lookup (\<lambda>x. x=k) al = Some v"
  by (induction al) (auto simp add: rev_image_eqI)

lemma lookup_None_iff: "lookup P xs = None \<longleftrightarrow> \<not> (\<exists>x. x \<in> set (map fst xs) \<and> P x)"
  by (induction xs) (auto split: if_splits)

(* This means lookup seems somewhat superflouus *)
lemma find_Some_imp_lookup_Some: 
  "List.find (\<lambda>(k,_). P k) xs = Some (k,v) \<Longrightarrow> lookup P xs = Some v"
  by (induction xs) auto

lemma lookup_Some_imp_find_Some: 
  "lookup P xs = Some v \<Longrightarrow> \<exists>x. List.find (\<lambda>(k,_). P k) xs = Some (x,v)"
  by (induction xs) auto

lemma lookup_None_iff_find_None: "lookup P xs = None \<longleftrightarrow> List.find (\<lambda>(k,_). P k) xs = None"
  by (induction xs) auto

lemma lookup_eq_order_irrelevant:
  assumes "distinct (map fst pairs)" and "distinct (map fst pairs')" and "set pairs = set pairs'"
  shows "lookup (\<lambda>x. x=k) pairs = lookup (\<lambda>x. x=k) pairs'"
proof (cases "lookup (\<lambda>x. x=k) pairs")
  case None
  then show ?thesis using lookup_None_iff
    by (metis assms(3) set_map)
next
  case (Some v)
  hence "(k,v)\<in>set pairs"
    by (metis (mono_tags) assms(1) lookup_None_iff lookup_present_eq_key map_of_SomeD map_of_eq_None_iff option.exhaust set_map)
  hence el: "(k,v)\<in>set pairs'" using assms(3) by blast
  show ?thesis using lookup_present_eq_key[OF assms(2) el] Some by simp
qed



end